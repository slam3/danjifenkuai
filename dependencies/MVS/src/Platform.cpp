/*
* Platform.cpp
*
* Copyright (c) 2014-2015 SEACAVE
*
* Author(s):
*
*      cDc <cdc.seacave@gmail.com>
*
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*
* Additional Terms:
*
*      You are required to preserve legal notices and author attributions in
*      that material or in the Appropriate Legal Notices displayed by works
*      containing it.
*/

#include "Common.h"
#include "Platform.h"

using namespace MVS;


// D E F I N E S ///////////////////////////////////////////////////


// S T R U C T S ///////////////////////////////////////////////////

// return the normalized absolute camera pose
Platform::Camera Platform::GetCamera(uint32_t cameraID, uint32_t poseID) const
{
	const Camera& camera = cameras[cameraID];
	const Pose& pose = poses[poseID];
	// add the relative camera pose to the platform
	Camera cam;
	cam.K = camera.K;
	cam.R = camera.R*pose.R;
	cam.C = pose.R.t()*camera.C+pose.C;
	return cam;
} // GetCamera
/*----------------------------------------------------------------*/


void BoundBox::Print()
{
	printf("[(%0.3f, %0.3f) x (%0.3f, %0.3f)]\n",
		m_xmin, m_ymin, m_xmax, m_ymax);

}

/* Returns the area of the bounding box */
double BoundBox::Area()
{
	return (m_xmax - m_xmin) * (m_ymax - m_ymin);
}


/* Returns true iff the box contains the given point */
bool BoundBox::Contains(double x, double y)
{
	return (x >= m_xmin && x <= m_xmax && y >= m_ymin && y <= m_ymax);
}

bool BoundBox::Contains(const BoundBox &bbox)
{
	return (bbox.m_xmin >= m_xmin &&
		bbox.m_xmax <= m_xmax &&
		bbox.m_ymin >= m_ymin &&
		bbox.m_ymax <= m_ymax);
}

void BoundBox::Scale(double scale)
{
	m_xmin *= scale;
	m_xmax *= scale;
	m_ymin *= scale;
	m_ymax *= scale;
}
