#include "Common.h"
#include "Scene.h"
#define _USE_OPENCV
#include "Interface.h"
#define  overlap 1.0/200
using namespace MVS;


// D E F I N E S ///////////////////////////////////////////////////

#define PROJECT_ID "MVS\0" // identifies the project stream
#define PROJECT_VER ((uint32_t)1) // identifies the version of a project stream


// S T R U C T S ///////////////////////////////////////////////////

void Scene::Release()
{
	platforms.Release();
	images.Release();
	pointcloud.Release();
	mesh.Release();
}

bool Scene::IsEmpty() const
{
	return pointcloud.IsEmpty() && mesh.IsEmpty();
}


bool Scene::LoadInterface(const String & fileName)
{
	TD_TIMER_STARTD();
	Interface obj;

	// serialize in the current state
	if (!ARCHIVE::SerializeLoad(obj, fileName))
		return false;

	// import platforms and cameras
	ASSERT(!obj.platforms.empty());
	platforms.Reserve((uint32_t)obj.platforms.size());
	for (Interface::PlatformArr::const_iterator itPlatform=obj.platforms.begin(); itPlatform!=obj.platforms.end(); ++itPlatform) {
		Platform& platform = platforms.AddEmpty();
		platform.name = itPlatform->name;
		platform.cameras.Reserve((uint32_t)itPlatform->cameras.size());
		for (Interface::Platform::CameraArr::const_iterator itCamera=itPlatform->cameras.begin(); itCamera!=itPlatform->cameras.end(); ++itCamera) {
			Platform::Camera& camera = platform.cameras.AddEmpty();
			camera.K = itCamera->K;
			camera.R = itCamera->R;
			camera.C = itCamera->C;
			if (!itCamera->IsNormalized()) {
				// normalize K
				ASSERT(itCamera->HasResolution());
				const REAL scale(REAL(1)/camera.GetNormalizationScale(itCamera->width,itCamera->height));
				camera.K(0,0) *= scale;
				camera.K(1,1) *= scale;
				camera.K(0,2) *= scale;
				camera.K(1,2) *= scale;
			}
			DEBUG_EXTRA("Camera model loaded: platform %u; camera %2u; f %.3fx%.3f; poses %u", platforms.GetSize()-1, platform.cameras.GetSize()-1, camera.K(0,0), camera.K(1,1), itPlatform->poses.size());
		}
		ASSERT(platform.cameras.GetSize() == itPlatform->cameras.size());
		platform.poses.Reserve((uint32_t)itPlatform->poses.size());
		for (Interface::Platform::PoseArr::const_iterator itPose=itPlatform->poses.begin(); itPose!=itPlatform->poses.end(); ++itPose) {
			Platform::Pose& pose = platform.poses.AddEmpty();
			pose.R = itPose->R;
			pose.C = itPose->C;
		}
		ASSERT(platform.poses.GetSize() == itPlatform->poses.size());
	}
	ASSERT(platforms.GetSize() == obj.platforms.size());
	if (platforms.IsEmpty())
		return false;

	// import images
	nCalibratedImages = 0;
	size_t nTotalPixels(0);
	ASSERT(!obj.images.empty());
	images.Reserve((uint32_t)obj.images.size());
	for (Interface::ImageArr::const_iterator it=obj.images.begin(); it!=obj.images.end(); ++it) {
		const Interface::Image& image = *it;
		const uint32_t ID(images.GetSize());
		Image& imageData = images.AddEmpty();
		imageData.name = image.name;
		Util::ensureUnifySlash(imageData.name);
		imageData.name = MAKE_PATH_FULL(WORKING_FOLDER_FULL, imageData.name);
		imageData.poseID = image.poseID;
		if (imageData.poseID == NO_ID) {
			DEBUG_EXTRA("warning: uncalibrated image '%s'", image.name.c_str());
			continue;
		}
		imageData.platformID = image.platformID;
		imageData.cameraID = image.cameraID;
		// init camera
		const Interface::Platform::Camera& camera = obj.platforms[image.platformID].cameras[image.cameraID];
		if (camera.HasResolution()) {
			// use stored resolution
			imageData.width = camera.width;
			imageData.height = camera.height;
			imageData.scale = 1;
		} else {
			// read image header for resolution
			if (!imageData.ReloadImage(0, false))
				return false;
		}
		imageData.UpdateCamera(platforms);
		++nCalibratedImages;
		nTotalPixels += imageData.width * imageData.height;
		DEBUG_EXTRA("Image loaded %3u: %s", ID, Util::getFileFullName(imageData.name).c_str());
	}
	if (images.GetSize() < 2)
		return false;

	// import 3D points
	if (!obj.vertices.empty()) {
		bool bValidWeights(false);
		pointcloud.points.Resize(obj.vertices.size());
		pointcloud.pointViews.Resize(obj.vertices.size());
		pointcloud.pointWeights.Resize(obj.vertices.size());
		FOREACH(i, pointcloud.points) {
			const Interface::Vertex& vertex = obj.vertices[i];
			PointCloud::Point& point = pointcloud.points[i];
			point = vertex.X;
			PointCloud::ViewArr& views = pointcloud.pointViews[i];
			views.Resize((PointCloud::ViewArr::IDX)vertex.views.size());
			PointCloud::WeightArr& weights = pointcloud.pointWeights[i];
			weights.Resize((PointCloud::ViewArr::IDX)vertex.views.size());
			CLISTDEF0(PointCloud::ViewArr::IDX) indices(views.GetSize());
			std::iota(indices.Begin(), indices.End(), 0);
			std::sort(indices.Begin(), indices.End(), [&](IndexArr::Type i0, IndexArr::Type i1) -> bool {
				return vertex.views[i0].imageID < vertex.views[i1].imageID;
			});
			ASSERT(vertex.views.size() >= 2);
			views.ForEach([&](PointCloud::ViewArr::IDX v) {
				const Interface::Vertex::View& view = vertex.views[indices[v]];
				views[v] = view.imageID;
				weights[v] = view.confidence;
				if (view.confidence != 0)
					bValidWeights = true;
			});
		}
		if (!bValidWeights)
			pointcloud.pointWeights.Release();
		if (!obj.verticesNormal.empty()) {
			ASSERT(obj.vertices.size() == obj.verticesNormal.size());
			pointcloud.normals.CopyOf((const Point3f*)&obj.verticesNormal[0].n, obj.vertices.size());
		}
		if (!obj.verticesColor.empty()) {
			ASSERT(obj.vertices.size() == obj.verticesColor.size());
			pointcloud.colors.CopyOf((const Pixel8U*)&obj.verticesColor[0].c, obj.vertices.size());
		}
	}

	DEBUG_EXTRA("Scene loaded from interface format (%s):\n"
				"\t%u images (%u calibrated) with a total of %.2f MPixels (%.2f MPixels/image)\n"
				"\t%u points, %u vertices, %u faces",
				TD_TIMER_GET_FMT().c_str(),
				images.GetSize(), nCalibratedImages, (double)nTotalPixels/(1024.0*1024.0), (double)nTotalPixels/(1024.0*1024.0*nCalibratedImages),
				pointcloud.points.GetSize(), mesh.vertices.GetSize(), mesh.faces.GetSize());
	return true;
} // LoadInterface

bool Scene::SaveInterface(const String & fileName) const
{
	TD_TIMER_STARTD();
	Interface obj;

	// export platforms
	obj.platforms.reserve(platforms.GetSize());
	FOREACH(i, platforms) 
	{
		const Platform& platform = platforms[i];
		Interface::Platform plat;
		plat.cameras.reserve(platform.cameras.GetSize());
		
		FOREACH(j, platform.cameras) 
		{
			const Platform::Camera& camera = platform.cameras[j];
			Interface::Platform::Camera cam;
			cam.K = camera.K;
			cam.R = camera.R;
			cam.C = camera.C;
			plat.cameras.push_back(cam);
		}

		plat.poses.reserve(platform.poses.GetSize());
		FOREACH(j, platform.poses) {
			const Platform::Pose& pose = platform.poses[j];
			Interface::Platform::Pose p;
			p.R = pose.R;
			p.C = pose.C;
			plat.poses.push_back(p);
		}
		obj.platforms.push_back(plat);
	}

	// export images
	obj.images.resize(images.GetSize());
	FOREACH(i, images) {
		const Image& imageData = images[i];
		MVS::Interface::Image& image = obj.images[i];
		image.name = MAKE_PATH_REL(WORKING_FOLDER_FULL, imageData.name);
		image.poseID = imageData.poseID;
		image.platformID = imageData.platformID;
		image.cameraID = imageData.cameraID;
	}

	// export 3D points
	obj.vertices.resize(pointcloud.points.GetSize());
	FOREACH(i, pointcloud.points) {
		const PointCloud::Point& point = pointcloud.points[i];
		const PointCloud::ViewArr& views = pointcloud.pointViews[i];
		MVS::Interface::Vertex& vertex = obj.vertices[i];
		ASSERT(sizeof(vertex.X.x) == sizeof(point.x));
		vertex.X = point;
		vertex.views.resize(views.GetSize());
		views.ForEach([&](PointCloud::ViewArr::IDX v) {
			MVS::Interface::Vertex::View& view = vertex.views[v];
			view.imageID = views[v];
			view.confidence = (pointcloud.pointWeights.IsEmpty() ? 0.f : pointcloud.pointWeights[i][v]);
		});
	}
	if (!pointcloud.normals.IsEmpty()) {
		obj.verticesNormal.resize(pointcloud.normals.GetSize());
		FOREACH(i, pointcloud.normals) {
			const PointCloud::Normal& normal = pointcloud.normals[i];
			MVS::Interface::Normal& vertexNormal = obj.verticesNormal[i];
			vertexNormal.n = normal;
		}
	}
	if (!pointcloud.normals.IsEmpty()) {
		obj.verticesColor.resize(pointcloud.colors.GetSize());
		FOREACH(i, pointcloud.colors) {
			const PointCloud::Color& color = pointcloud.colors[i];
			MVS::Interface::Color& vertexColor = obj.verticesColor[i];
			vertexColor.c = color;
		}
	}
	// serialize out the current state
	if (!ARCHIVE::SerializeSave(obj, fileName))
		return false;

	DEBUG_EXTRA("Scene saved to interface format (%s):\n"
				"\t%u images (%u calibrated)\n"
				"\t%u points, %u vertices, %u faces",
				TD_TIMER_GET_FMT().c_str(),
				images.GetSize(), nCalibratedImages,
				pointcloud.points.GetSize(), mesh.vertices.GetSize(), mesh.faces.GetSize());
	return true;
} // SaveInterface
/*----------------------------------------------------------------*/

bool Scene::Load(const String& fileName)
{
	TD_TIMER_STARTD();
	Release();

	#ifdef _USE_BOOST
	// open the input stream
	std::ifstream fs(fileName, std::ios::in | std::ios::binary);
	if (!fs.is_open())
		return false;
	// load project header ID
	char szHeader[4];
	fs.read(szHeader, 4);
	if (!fs || _tcsncmp(szHeader, PROJECT_ID, 4) != 0) {
		fs.close();
		if (LoadInterface(fileName))
			return true;
		VERBOSE("error: invalid project");
		return false;
	}
	// load project version
	uint32_t nVer;
	fs.read((char*)&nVer, sizeof(uint32_t));
	if (!fs || nVer != PROJECT_VER) {
		VERBOSE("error: different project version");
		return false;
	}
	// load stream type
	uint32_t nType;
	fs.read((char*)&nType, sizeof(uint32_t));
	// skip reserved bytes
	uint64_t nReserved;
	fs.read((char*)&nReserved, sizeof(uint64_t));
	// serialize in the current state
	if (!SerializeLoad(*this, fs, (ARCHIVE_TYPE)nType))
		return false;
	// init images
	nCalibratedImages = 0;
	size_t nTotalPixels(0);
	FOREACH(ID, images) {
		Image& imageData = images[ID];
		if (imageData.poseID == NO_ID)
			continue;
		imageData.UpdateCamera(platforms);
		++nCalibratedImages;
		nTotalPixels += imageData.width * imageData.height;
	}
	DEBUG_EXTRA("Scene loaded (%s):\n"
				"\t%u images (%u calibrated) with a total of %.2f MPixels (%.2f MPixels/image)\n"
				"\t%u points, %u vertices, %u faces",
				TD_TIMER_GET_FMT().c_str(),
				images.GetSize(), nCalibratedImages, (double)nTotalPixels/(1024.0*1024.0), (double)nTotalPixels/(1024.0*1024.0*nCalibratedImages),
				pointcloud.points.GetSize(), mesh.vertices.GetSize(), mesh.faces.GetSize());
	return true;
	#else
	return false;
	#endif
} // Load

bool Scene::Save(const String& fileName, ARCHIVE_TYPE type) const
{
	TD_TIMER_STARTD();
	// save using MVS interface if requested
	if (type == ARCHIVE_MVS) {
		if (mesh.IsEmpty())
			return SaveInterface(fileName);
		type = ARCHIVE_BINARY_ZIP;
	}
	#ifdef _USE_BOOST
	// open the output stream
	std::ofstream fs(fileName, std::ios::out | std::ios::binary);
	if (!fs.is_open())
		return false;
	// save project ID
	fs.write(PROJECT_ID, 4);
	// save project version
	const uint32_t nVer(PROJECT_VER);
	fs.write((const char*)&nVer, sizeof(uint32_t));
	// save stream type
	const uint32_t nType = type;
	fs.write((const char*)&nType, sizeof(uint32_t));
	// reserve some bytes
	const uint64_t nReserved = 0;
	fs.write((const char*)&nReserved, sizeof(uint64_t));
	// serialize out the current state
	if (!SerializeSave(*this, fs, type))
		return false;
	DEBUG_EXTRA("Scene saved (%s):\n"
				"\t%u images (%u calibrated)\n"
				"\t%u points, %u vertices, %u faces",
				TD_TIMER_GET_FMT().c_str(),
				images.GetSize(), nCalibratedImages,
				pointcloud.points.GetSize(), mesh.vertices.GetSize(), mesh.faces.GetSize());
	return true;
	#else
	return false;
	#endif
} // Save
/*----------------------------------------------------------------*/



inline float Footprint(const Camera& camera, const Point3f& X) {
	const REAL fSphereRadius(1);
	const Point3 cX(camera.TransformPointW2C(Cast<REAL>(X)));
	return (float)norm(camera.TransformPointC2I(Point3(cX.x+fSphereRadius,cX.y,cX.z))-camera.TransformPointC2I(cX))+std::numeric_limits<float>::epsilon();
}

// compute visibility for the reference image
// and select the best views for reconstructing the dense point-cloud;
// extract also all 3D points seen by the reference image;
// (inspired by: "Multi-View Stereo for Community Photo Collections", Goesele, 2007)
bool Scene::SelectNeighborViews(uint32_t ID, IndexArr& points, unsigned nMinViews, unsigned nMinPointViews, float fOptimAngle)
{
	ASSERT(points.IsEmpty());

	// extract the estimated 3D points and the corresponding 2D projections for the reference image
	Image& imageData = images[ID];
	ASSERT(imageData.IsValid());
	ViewScoreArr& neighbors = imageData.neighbors;
	ASSERT(neighbors.IsEmpty());
	struct Score {
		float score;
		float avgScale;
		float avgAngle;
		uint32_t points;
	};
	CLISTDEF0(Score) scores(images.GetSize());
	scores.Memset(0);
	if (nMinPointViews > nCalibratedImages)
		nMinPointViews = nCalibratedImages;
	unsigned nPoints = 0;
	imageData.avgDepth = 0;
	FOREACH(idx, pointcloud.points) {
		const PointCloud::ViewArr& views = pointcloud.pointViews[idx];
		ASSERT(views.IsSorted());
		if (views.FindFirst(ID) == PointCloud::ViewArr::NO_INDEX)
			continue;
		// store this point
		const PointCloud::Point& point = pointcloud.points[idx];
		if (views.GetSize() >= nMinPointViews)
			points.Insert((uint32_t)idx);
		imageData.avgDepth += (float)imageData.camera.PointDepth(point);
		++nPoints;
		// score shared views
		const Point3f V1(imageData.camera.C - Cast<REAL>(point));
		const float footprint1(Footprint(imageData.camera, point));
		FOREACHPTR(pView, views) {
			const PointCloud::View& view = *pView;
			if (view == ID)
				continue;
			const Image& imageData2 = images[view];
			const Point3f V2(imageData2.camera.C - Cast<REAL>(point));
			const float footprint2(Footprint(imageData2.camera, point));
			const float fAngle(ACOS(ComputeAngle<float,float>(V1.ptr(), V2.ptr())));
			const float fScaleRatio(footprint1/footprint2);
			const float wAngle(MINF(POW(fAngle/fOptimAngle, 1.5f), 1.f));
			float wScale;
			if (fScaleRatio > 1.6f)
				wScale = SQUARE(1.6f/fScaleRatio);
			else if (fScaleRatio >= 1.f)
				wScale = 1.f;
			else
				wScale = SQUARE(fScaleRatio);
			Score& score = scores[view];
			score.score += wAngle * wScale;
			score.avgScale += fScaleRatio;
			score.avgAngle += fAngle;
			++score.points;
		}
	}
	imageData.avgDepth /= nPoints;
	ASSERT(nPoints > 3);

	// select best neighborViews
	Point2fArr pointsA(0, points.GetSize()), pointsB(0, points.GetSize());
	FOREACH(IDB, images) {
		const Image& imageDataB = images[IDB];
		if (!imageDataB.IsValid())
			continue;
		const Score& score = scores[IDB];
		if (score.points == 0)
			continue;
		ASSERT(ID != IDB);
		ViewScore& neighbor = neighbors.AddEmpty();
		// compute how well the matched features are spread out (image covered area)
		const Point2f boundsA(imageData.GetSize());
		const Point2f boundsB(imageDataB.GetSize());
		ASSERT(pointsA.IsEmpty() && pointsB.IsEmpty());
		FOREACHPTR(pIdx, points) {
			const PointCloud::ViewArr& views = pointcloud.pointViews[*pIdx];
			ASSERT(views.IsSorted());
			ASSERT(views.FindFirst(ID) != PointCloud::ViewArr::NO_INDEX);
			if (views.FindFirst(IDB) == PointCloud::ViewArr::NO_INDEX)
				continue;
			const PointCloud::Point& point = pointcloud.points[*pIdx];
			Point2f& ptA = pointsA.AddConstruct(imageData.camera.ProjectPointP(point));
			Point2f& ptB = pointsB.AddConstruct(imageDataB.camera.ProjectPointP(point));
			if (!imageData.camera.IsInside(ptA, boundsA) || !imageDataB.camera.IsInside(ptB, boundsB)) {
				pointsA.RemoveLast();
				pointsB.RemoveLast();
			}
		}
		ASSERT(pointsA.GetSize() == pointsB.GetSize() && pointsA.GetSize() <= score.points);
		const float areaA(ComputeCoveredArea<float, 2, 16, false>((const float*)pointsA.Begin(), pointsA.GetSize(), boundsA.ptr()));
		const float areaB(ComputeCoveredArea<float, 2, 16, false>((const float*)pointsB.Begin(), pointsB.GetSize(), boundsB.ptr()));
		const float area(MINF(areaA, areaB));
		pointsA.Empty(); pointsB.Empty();
		// store image score
		neighbor.idx.ID = IDB;
		neighbor.idx.points = score.points;
		neighbor.idx.scale = score.avgScale/score.points;
		neighbor.idx.angle = score.avgAngle/score.points;
		neighbor.idx.area = area;
		neighbor.score = score.score*area;
	}
	neighbors.Sort();
	#if TD_VERBOSE != TD_VERBOSE_OFF
	// print neighbor views
	if (VERBOSITY_LEVEL > 2) {
		String msg;
		FOREACH(n, neighbors)
			msg += String::FormatString(" %3u(%upts,%.2fscl)", neighbors[n].idx.ID, neighbors[n].idx.points, neighbors[n].idx.scale);
		VERBOSE("Reference image %3u sees %u views:%s (%u shared points)", ID, neighbors.GetSize(), msg.c_str(), nPoints);
	}
	#endif
	if (points.GetSize() <= 3 || neighbors.GetSize() < MINF(nMinViews,nCalibratedImages-1)) {
		DEBUG_EXTRA("error: reference image %3u has not enough images in view", ID);
		return false;
	}
	return true;
} // SelectNeighborViews
/*----------------------------------------------------------------*/

// keep only the best neighbors for the reference image
bool Scene::FilterNeighborViews(ViewScoreArr& neighbors, float fMinArea, float fMinScale, float fMaxScale, float fMinAngle, float fMaxAngle, unsigned nMaxViews)
{
	// remove invalid neighbor views
	RFOREACH(n, neighbors) {
		const ViewScore& neighbor = neighbors[n];
		if (neighbor.idx.area < fMinArea ||
			!ISINSIDE(neighbor.idx.scale, fMinScale, fMaxScale) ||
			!ISINSIDE(neighbor.idx.angle, fMinAngle, fMaxAngle))
			neighbors.RemoveAtMove(n);
	}
	if (neighbors.GetSize() > nMaxViews)
		neighbors.Resize(nMaxViews);
	return !neighbors.IsEmpty();
} // FilterNeighborViews
/*----------------------------------------------------------------*/
//
//void DealVector(std::vector<int>images, std::vector<int>&imageIdx, std::vector<int>&image_Num)
void DealVector(std::vector<int>images, std::vector<int>&imageIdx)
{
	//
	//std::vector<int>image_Num2;
	int i, j;
	int f = -1;
	for (i = 0; i < images.size() - 1;)
	{
		j = i + 1;
		while (j<images.size() && images[j] == images[i])
		{
			images[j] = f;
			j++;
		}
		i = j;
	}

	for (int i = 0; i < images.size(); i++)
	{
		if (images[i] != f)
			imageIdx.push_back(images[i]);
	}
	//image_Num2.resize(imageIdx.size()+1);

	//for (int k = 0; k < imageIdx.size(); k++)
	//{
	//	std::vector<int>::iterator iElement = find(images.begin(),
	//	images.end(), imageIdx[k]);

	//	if (iElement != images.end())
	//	{
	//		int nPosition2 = distance(images.begin(), iElement);
	//		image_Num2[k] = nPosition2;
	//	}
	//}
	//image_Num2[imageIdx.size()]=images.size();

	//for (int j = 0; j < image_Num2.size() - 1; j++)
	//{
	//	image_Num.push_back(image_Num2[j+1]-image_Num2[j]);
	//}

}

bool  Scene::SetBoundingBox(BoundBox &box)
{
	bb.m_xmin = box.m_xmin;
	bb.m_xmax = box.m_xmax;
	bb.m_ymin = box.m_ymin;
	bb.m_ymax = box.m_ymax;
	bb.m_zmax = box.m_zmax;
	bb.m_zmin = box.m_zmin;
	return true;
}
bool Scene::CreateBoundingBox()
{
	
	bb.m_xmin = DBL_MAX;
	bb.m_xmax = -DBL_MAX;
	bb.m_ymin = DBL_MAX;
	bb.m_ymax = -DBL_MAX;
	bb.m_zmax = DBL_MAX;
	bb.m_zmin = -DBL_MAX;

	std::vector<double> pointL;
	std::vector<double> pointH;
	std::vector<double> pointZ;

	if (pointcloud.points.empty() && mesh.vertices.empty())
	{
		VERBOSE("Make Sure The Input Data's Correction!");
		return false;
	}

	if (!pointcloud.points.empty())
	{
		for (size_t i = 0; i < pointcloud.points.size(); ++i)
		{
			pointL.push_back(pointcloud.points[i].x);
			pointH.push_back(pointcloud.points[i].y);
			pointZ.push_back(pointcloud.points[i].z);
		}
	}
	if (pointcloud.points.empty() && !mesh.vertices.empty())
	{
		for (size_t i = 0; i <mesh.vertices.size(); ++i)
		{
			pointL.push_back(mesh.vertices[i].x);
			pointH.push_back(mesh.vertices[i].y);
			pointZ.push_back(mesh.vertices[i].z); 
		}
	}
	if (pointL.size() > 0)
	{
		bb.m_xmin = *min_element(pointL.begin(), pointL.end());
		bb.m_xmax = *max_element(pointL.begin(), pointL.end());
		bb.m_ymin = *min_element(pointH.begin(), pointH.end());
		bb.m_ymax = *max_element(pointH.begin(), pointH.end());
		bb.m_zmin = *min_element(pointZ.begin(), pointZ.end());
		bb.m_zmax = *max_element(pointZ.begin(), pointZ.end());
		return true;
	}

	else
		return false;
	
}

bool Scene::CutScope(Scene& cut_scene)
{
	FOREACH(j, pointcloud.points)
	{
		if (pointcloud.points[j].x <= bb_suit.m_xmax &&pointcloud.points[j].x >= bb_suit.m_xmin &&pointcloud.points[j].y <= bb_suit.m_ymax
			&&pointcloud.points[j].y >= bb_suit.m_ymin&&pointcloud.points[j].z >= bb_suit.m_zmin&&pointcloud.points[j].z <= bb_suit.m_zmax)
		{
			cut_scene.pointcloud.points.push_back(pointcloud.points[j]);
			cut_scene.pointcloud.pointViews.push_back(pointcloud.pointViews[j]);
			if (!pointcloud.colors.empty())
				cut_scene.pointcloud.colors.push_back(pointcloud.colors[j]);
			if (!pointcloud.normals.empty())
				cut_scene.pointcloud.normals.push_back(pointcloud.normals[j]);
		}
		else
			continue;
	}
	std::vector<int>mimage, mimageIdx;

	for (int k = 0; k < cut_scene.pointcloud.pointViews.size(); ++k)
	{
		for (int h = 0; h <cut_scene.pointcloud.pointViews[k].size(); ++h)
		{
			mimage.push_back(cut_scene.pointcloud.pointViews[k][h]);
		}
	}

	std::sort(mimage.begin(), mimage.end());
	DealVector(mimage, mimageIdx);

	for (int k = 0; k <cut_scene.pointcloud.pointViews.size(); ++k)
	{
		for (int m = 0; m < cut_scene.pointcloud.pointViews[k].size(); ++m)
		{

			std::vector<int>::iterator iElement = find(mimageIdx.begin(),
				mimageIdx.end(), cut_scene.pointcloud.pointViews[k][m]);

			if (iElement != mimageIdx.end())
			{
				int nPosition2 = distance(mimageIdx.begin(), iElement);
				cut_scene.pointcloud.pointViews[k][m] = nPosition2;
			}
		}
	}

	cut_scene.images.Reserve((uint32_t)mimageIdx.size());
	cut_scene.nCalibratedImages = mimageIdx.size();
	for (int k = 0; k < mimageIdx.size(); k++)
	{
		MVS::Image& sub_image = cut_scene.images.AddEmpty();
		sub_image.name = images[mimageIdx.at(k)].name;
		//	sub_image.cameraID = k;
		if (!sub_image.ReloadImage(0, false)) {
			LOG("error: can not read image %s", sub_image.name.c_str());
			return EXIT_FAILURE;
		}

		sub_image.platformID = cut_scene.platforms.GetSize();

		MVS::Platform& sub_platform = cut_scene.platforms.AddEmpty();
		//MVS::Platform::Camera& sub_camera = sub_platform.cameras.AddEmpty();
		sub_platform.cameras.Reserve(platforms[images[mimageIdx.at(k)].platformID].cameras.size());
		FOREACH(jj, platforms[images[mimageIdx.at(k)].platformID].cameras)
		{

			MVS::Platform::Camera& sub_camera = sub_platform.cameras.AddEmpty();
			sub_camera = platforms[images[mimageIdx.at(k)].platformID].cameras[jj];
		}
		sub_image.cameraID = 0;
		sub_image.poseID = sub_platform.poses.GetSize();
		MVS::Platform::Pose & sub_pose = sub_platform.poses.AddEmpty();
		sub_pose = platforms[images[mimageIdx.at(k)].platformID].poses[images[mimageIdx.at(k)].poseID];
		sub_image.UpdateCamera(cut_scene.platforms);
	}

	if (!cut_scene.pointcloud.points.empty())
		return true;
	else
	{
		VERBOSE("DesifyPoint Cut Scope Failure!");
		return false;
	}
}
bool Scene::CreateSuitBoundingBox(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered)
{
	if (pointcloud.points.empty() && mesh.vertices.empty())
	{
		VERBOSE("Make Sure The Input Data's Correction!");
		return false;
	}
	
	if (!pointcloud.points.empty())
	{
		cloud->points.resize(pointcloud.points.size());
		for (size_t i = 0; i < pointcloud.points.size(); ++i)
		{
			cloud->points[i].x = pointcloud.points[i].x;
			cloud->points[i].y = pointcloud.points[i].y;
			cloud->points[i].z = pointcloud.points[i].z;
		}
	}
	if (pointcloud.points.empty() && !mesh.vertices.empty())
	{
		cloud->points.resize(mesh.vertices.size());
		for (size_t i = 0; i <mesh.vertices.size(); ++i)
		{
			cloud->points[i].x = mesh.vertices[i].x;
			cloud->points[i].y = mesh.vertices[i].y;
			cloud->points[i].z = mesh.vertices[i].z;
		}
	}
	pcl::RadiusOutlierRemoval<pcl::PointXYZ> outrem;
	// build the filter
	outrem.setInputCloud(cloud);
	outrem.setRadiusSearch(0.1);
	outrem.setMinNeighborsInRadius(25);
	// apply filter
	outrem.filter(*cloud_filtered);
	
//	BoundBox bb2;
	bb.m_xmin = -DBL_MAX;
	bb.m_xmax = DBL_MAX;
	bb.m_ymin = -DBL_MAX;
	bb.m_ymax = DBL_MAX;
	bb.m_zmin = -DBL_MAX;
	bb.m_zmax = DBL_MAX;
	int num_points = cloud_filtered->size();
	if (num_points == 0) {
		printf("[CreateBoundingBox] No points given!\n");
		// bb.m_xmin = bb.m_xmax = bb.m_ymin = bb.m_ymin = 0.0;
		return false;
	}

	std::vector<float> pointL;
	pointL.resize(num_points);
	std::vector<float> pointH;
	pointH.resize(num_points);

	std::vector<float> pointZ;
	pointZ.resize(num_points);
	//	zlabel.resize(num_points);
	for (int i = 0; i < num_points; i++)
	{
		pointL[i]=cloud_filtered->points[i].x;
		pointH[i]=cloud_filtered->points[i].y;
		pointZ[i]=cloud_filtered->points[i].z;
	}

	if (pointL.size() > 0)
	{
		bb.m_xmin = *min_element(pointL.begin(), pointL.end());
		bb.m_xmax = *max_element(pointL.begin(), pointL.end());
		bb.m_ymin = *min_element(pointH.begin(), pointH.end());
		bb.m_ymax = *max_element(pointH.begin(), pointH.end());
		bb.m_zmin = *min_element(pointZ.begin(), pointZ.end());
		bb.m_zmax = *max_element(pointZ.begin(), pointZ.end());
		return true;
	}

	else
		return false;
}

bool Scene::CreateSubBoundingBox(int scale)    ///设置大一点的包围盒
{
	//BoundBox bb2;
	double yL = bb.Height();
	double xL = bb.Width();
	bb_suit.m_xmin = bb.m_xmin - xL / scale;
	bb_suit.m_xmax = bb.m_xmax + xL / scale;
	bb_suit.m_ymin = bb.m_ymin - yL / scale;
	bb_suit.m_ymax = bb.m_ymax + yL / scale;
	bb_suit.m_zmax = bb.m_zmax;
	bb_suit.m_zmin = bb.m_zmin;
	return true;
}

bool Scene::CreateConBoundingBox(int scale)   ///设置大一点的包围盒；scale为负值时 是减小包围盒
{
	//BoundBox bb2;
	double yL = bb.Height();
	double xL = bb.Width();
	bb.m_xmin = bb.m_xmin - xL / scale;
	bb.m_xmax = bb.m_xmax + xL / scale;
	bb.m_ymin = bb.m_ymin - yL / scale;
	bb.m_ymax = bb.m_ymax + yL / scale;
	bb.m_zmax = bb.m_zmax;
	bb.m_zmin = bb.m_zmin;
	return true;
}

bool Scene::Devide(std::vector<Scene>&sub_scene, int col, int row)
{
	int num_block = col*row;

	std::vector<std::vector<int>> mimage;
	std::vector<std::vector<int>> mimageIdx;
	std::vector<std::vector<int>> mimageIdx_Num;
	std::vector<std::vector<int>> mimageNum;
	std::vector<Scene>sub_scene_scopes;

	sub_scene_scopes.resize(num_block);
	mimage.resize(num_block);
	mimageIdx.resize(num_block);
	mimageIdx_Num.resize(num_block);
    mimageNum.resize(num_block);
	double dely = bb.Height() / row;
	double delx = bb.Width() / col;
	if (!pointcloud.points.empty())
	{
		FOREACH(Idx, pointcloud.points)
		{
			for (int t = 0; t < col; t++)
			{
				for (int k = 0; k < row; k++)
				{
					if (pointcloud.points[Idx].x >= (bb.m_xmin + delx*t-delx*overlap) && pointcloud.points[Idx].x<=(bb.m_xmin + delx*(t + 1)+ delx*overlap)
						&& pointcloud.points[Idx].y>=(bb.m_ymin + dely*k- dely*overlap) && pointcloud.points[Idx].y <=(bb.m_ymin + dely*(k + 1)+ dely*overlap))
					{
						sub_scene_scopes[t*row + k].pointcloud.points.push_back(pointcloud.points[Idx]);
						sub_scene_scopes[t*row + k].pointcloud.pointViews.push_back(pointcloud.pointViews[Idx]);
						if (!pointcloud.colors.empty())
						{
							sub_scene_scopes[t*row + k].pointcloud.colors.push_back(pointcloud.colors[Idx]);
						}
						if (!pointcloud.normals.empty())
						{
							sub_scene_scopes[t*row + k].pointcloud.normals.push_back(pointcloud.normals[Idx]);
						}
					}

					else
						continue;
				}
			}
		}

		for (int jj = 0; jj < num_block; jj++)
		{
			sub_scene_scopes[jj].CreateBoundingBox();
			//sub_scene_scopes[jj].CreateSubBoundingBox(3);
			sub_scene[jj].SetBoundingBox(sub_scene_scopes[jj].bb);
			sub_scene[jj].CreateSubBoundingBox(10); //扩大范围；；
		}


		for (int t = 0; t < col; t++)
		{
			for (int k = 0; k < row; k++)
			{
				FOREACH(Idx, pointcloud.points)
				{

					if (pointcloud.points[Idx].x >= sub_scene[t*row + k].bb_suit.m_xmin  && pointcloud.points[Idx].x<=sub_scene[t*row + k].bb_suit.m_xmax
						&& pointcloud.points[Idx].y>=sub_scene[t*row + k].bb_suit.m_ymin  && pointcloud.points[Idx].y <= sub_scene[t*row + k].bb_suit.m_ymax)
					{
						sub_scene[t*row + k].pointcloud.points.push_back(pointcloud.points[Idx]);
						sub_scene[t*row + k].pointcloud.pointViews.push_back(pointcloud.pointViews[Idx]);
						if (!pointcloud.colors.empty())
						{
							sub_scene[t*row + k].pointcloud.colors.push_back(pointcloud.colors[Idx]);
						}
						if (!pointcloud.normals.empty())
						{
							sub_scene[t*row + k].pointcloud.normals.push_back(pointcloud.normals[Idx]);
						}
					}

					else
						continue;
				}
			}
		}


		/*for (int jj = 0; jj < num_block; jj++)
		{
			sub_scene[jj].SetBoundingBox(sub_scene_scopes[jj].bb);
		}*/  //设置成了扩大后的版本  有毛病啊；

		for (int j = 0; j < num_block; j++)
		{
			for (int k = 0; k < sub_scene[j].pointcloud.pointViews.size(); k++)
			{
				for (int h = 0; h < sub_scene[j].pointcloud.pointViews[k].size(); h++)
				{
					mimage[j].push_back(sub_scene[j].pointcloud.pointViews[k][h]);
				}
			}
		}

		for (int j = 0; j < mimage.size(); j++)
		{
			sort(mimage[j].begin(), mimage[j].end());
		/*	DealVector(mimage[j], mimageIdx[j],mimageNum[j]);*/
			DealVector(mimage[j], mimageIdx[j]);
			//for (int kk = 0; kk < mimageIdx[j].size(); kk++)
			//{
			//	if (mimageNum[j][kk]> 1816)
			//		mimageIdx_Num[j].push_back(mimageIdx[j][kk]);//将大于500个点的影像存储下来；
			//	else
			//		continue;
			//}

		}
		//for (int j = 0; j < num_block; j++)
		//{
		//	for (int k = 0; k < sub_scene[j].pointcloud.pointViews.size(); k++)
		//	{
		//		for (int m = 0; m < sub_scene[j].pointcloud.pointViews[k].size(); m++)
		//		{
		//			std::vector<int>::iterator iElement = find(mimageIdx[j].begin(),
		//				mimageIdx[j].end(), sub_scene[j].pointcloud.pointViews[k][m]);//mimageIdx中包含很多少于100点的影像，将其对应的点以及pointview删掉；

		//			if (iElement != mimageIdx[j].end())
		//			{
		//				int nPosition = distance(mimageIdx[j].begin(), iElement);
		//				if (mimageNum[j][nPosition] < 1816)
		//				{
		//					sub_scene[j].pointcloud.pointViews.RemoveAt(k);
		//					sub_scene[j].pointcloud.points.RemoveAt(k);
		//					break;
		//				}
		//			}
		//		}
		//	}
		//}

		for (int j = 0; j < num_block; j++)
		{
			for (int k = 0; k < sub_scene[j].pointcloud.pointViews.size(); k++)
			{
				for (int m = 0; m < sub_scene[j].pointcloud.pointViews[k].size(); m++)
				{

	    		std::vector<int>::iterator iElement = find(mimageIdx[j].begin(),
						mimageIdx[j].end(), sub_scene[j].pointcloud.pointViews[k][m]);

					if (iElement != mimageIdx[j].end())
					{
						int nPosition2 = distance(mimageIdx[j].begin(), iElement);				
						sub_scene[j].pointcloud.pointViews[k][m] = nPosition2;				
					}
				}
			}
		}

		//for (int n = 0; n < num_block; n++)
		//{

		//	sub_scene[n].platforms.Reserve((uint32_t)mimageIdx[n].size());
		//	sub_scene[n].images.Reserve((uint32_t)mimageIdx[n].size());
		//	sub_scene[n].nCalibratedImages = mimageIdx[n].size();

		//	for (int k = 0; k < mimageIdx[n].size(); k++)
		//	{
		//		const Image& image = images[mimageIdx[n].at(k)];
		//		Image& imageData = sub_scene[n].images.AddEmpty();
		//		imageData.name = image.name;

		//		Platform& platform = sub_scene[n].platforms.AddEmpty();
		//		platform.name = platforms[images[mimageIdx[n].at(k)].platformID].name;
		//		platform.cameras.Reserve((uint32_t)platforms[images[mimageIdx[n].at(k)].platformID].cameras.size());
		//		for (Platform::CameraArr::const_iterator itCamera = platforms[images[mimageIdx[n].at(k)].platformID].cameras.begin(); itCamera != platforms[images[mimageIdx[n].at(k)].platformID].cameras.end(); ++itCamera) {
		//			Platform::Camera& camera = platform.cameras.AddEmpty();
		//			camera.K = itCamera->K;
		//			camera.R = itCamera->R;
		//			camera.C = itCamera->C;
		//			//	DEBUG_EXTRA("Camera model loaded: platform %u; camera %2u; f %.3fx%.3f; poses %u", platforms.GetSize() - 1, platform.cameras.GetSize() - 1, camera.K(0, 0), camera.K(1, 1), itPlatform->poses.size());
		//		}
		//	/*	FOREACH(jj, platforms[images[mimageIdx[n].at(k)].platformID].cameras)
		//		{
		//			MVS::Platform::Camera& sub_camera = platform.cameras.AddEmpty();
		//			sub_camera = platforms[images[mimageIdx[n].at(k)].platformID].cameras[jj];
		//		}*/
		//		ASSERT(platform.cameras.GetSize() == itPlatform->cameras.size());
		//	//	platform.poses.Reserve((uint32_t)platforms[images[mimageIdx[n].at(k)].platformID].poses.size());
		//		for (Platform::PoseArr::const_iterator itPose = platforms[images[mimageIdx[n].at(k)].platformID].poses.begin(); itPose != platforms[images[mimageIdx[n].at(k)].platformID].poses.end(); ++itPose) {
		//			imageData.poseID = platform.poses.GetSize();
		//			Platform::Pose& pose = platform.poses.AddEmpty();
		//			//sub_pose= platforms[images[mimageIdx[n].at(k)].platformID].poses[images[mimageIdx[n].at(k)].poseID];
		//			pose.R = itPose->R;
		//			pose.C = itPose->C;
		//		}
		//		imageData.platformID = image.platformID;
		//		imageData.cameraID = image.cameraID;

		//		imageData.width = image.width;
		//		imageData.height = image.height;
		//		imageData.scale = 1;
		//		imageData.UpdateCamera(sub_scene[n].platforms);
		//		ASSERT(platform.poses.GetSize() == itPlatform->poses.size());
		//	}

		//}


		//	for (int k = 0; k < mimageIdx[n].size(); k++)
		//	{
		//		const Image& image = images[mimageIdx[n].at(k)];
		//		Image& imageData = sub_scene[n].images.AddEmpty();
		//		imageData.name = image.name;
		//		//Util::ensureUnifySlash(imageData.name);
		//		//imageData.name = MAKE_PATH_FULL(WORKING_FOLDER_FULL, imageData.name);
		//		imageData.poseID = image.poseID;
		//		if (imageData.poseID == NO_ID) {
		//			DEBUG_EXTRA("warning: uncalibrated image '%s'", image.name.c_str());
		//			continue;
		//		}
		//		imageData.platformID = image.platformID;
		//		imageData.cameraID = image.cameraID;
		//		
		//		imageData.width = image.width;
		//		imageData.height = image.height;
		//		imageData.scale = 1;
		//		imageData.UpdateCamera(sub_scene[n].platforms);

		//	}
		//}

		for (int n = 0; n < num_block; n++)
		{
			sub_scene[n].images.Reserve((uint32_t)mimageIdx[n].size());
			sub_scene[n].nCalibratedImages = mimageIdx[n].size();
			for (int k = 0; k < mimageIdx[n].size(); k++)
			{
				MVS::Image& sub_image = sub_scene[n].images.AddEmpty();
				sub_image.name = images[mimageIdx[n].at(k)].name;
				//	sub_image.cameraID = k;
				if (!sub_image.ReloadImage(0, false)) {
					LOG("error: can not read image %s", sub_image.name.c_str());
					return EXIT_FAILURE;
				}

				sub_image.platformID = sub_scene[n].platforms.GetSize();

				MVS::Platform& sub_platform = sub_scene[n].platforms.AddEmpty();
				//MVS::Platform::Camera& sub_camera = sub_platform.cameras.AddEmpty();
				sub_platform.cameras.Reserve(platforms[images[mimageIdx[n].at(k)].platformID].cameras.size());
				FOREACH(jj, platforms[images[mimageIdx[n].at(k)].platformID].cameras)
				{

					MVS::Platform::Camera& sub_camera = sub_platform.cameras.AddEmpty();
					sub_camera = platforms[images[mimageIdx[n].at(k)].platformID].cameras[jj];
				}
				sub_image.cameraID = 0;
				sub_image.poseID = sub_platform.poses.GetSize();
				MVS::Platform::Pose & sub_pose = sub_platform.poses.AddEmpty();
				sub_pose = platforms[images[mimageIdx[n].at(k)].platformID].poses[images[mimageIdx[n].at(k)].poseID];
				sub_image.UpdateCamera(sub_scene[n].platforms);
			}
		}


	}
	
	if (!mesh.IsEmpty())
	{
		std::vector<std::map<int, Point3f>>sub_vertex;
		sub_vertex.resize(num_block);
		std::vector<FaceList> sub_face;
		sub_face.resize(num_block);
		std::vector<std::map<int, Vec3f>>sub_normal;
		sub_normal.resize(num_block);

		std::vector<std::map<int, int>> vexIdxold2New;
		vexIdxold2New.resize(num_block);
		int num_tri = mesh.faces.size();
		for (int i = 0; i < num_tri; i++)
		{
			//		Point3f PntA = mesh.faces[i];
			int A = mesh.faces[i].x;
			int B = mesh.faces[i].y;
			int C = mesh.faces[i].z;

			Point3f pntA = mesh.vertices[A];
			Point3f pntB = mesh.vertices[B];
			Point3f pntC = mesh.vertices[C];
			Point3f centerP = (pntA + pntB + pntC) / 3;
			for (int t = 0; t < col; t++)
			{
				for (int k = 0; k < row; k++)
				{
					if (centerP.x >= (bb.m_xmin + delx*t-delx*overlap) && centerP.x<=(bb.m_xmin + delx*(t + 1)+ delx*overlap)
						&& centerP.y>=(bb.m_ymin + dely*k- dely*overlap) && centerP.y <= (bb.m_ymin + dely*(k + 1))+ dely*overlap)
					{
						VertexID vt;
						vt.x = A;
						vt.y = B;
						vt.z = C;
						sub_face[t*row + k].push_back(vt);
					}
					else
						continue;
				}
			}
		}

		for (int h = 0; h < num_block; h++)
		{
			for (int g = 0; g < sub_face[h].size(); g++)
			{
				VertexID AA = sub_face[h].at(g);
				//sub_color[h][AA]=this->get_color_by_facenum(AA);
				sub_vertex[h][AA.x] = mesh.vertices[AA.x];
				sub_vertex[h][AA.y] = mesh.vertices[AA.y];
				sub_vertex[h][AA.z] = mesh.vertices[AA.z];
			}
		}
		for (int s = 0; s < num_block; s++)
		{
			std::map<int, Point3f>::iterator iterVex = sub_vertex[s].begin();
			int numC = 0;
			for (; iterVex != sub_vertex[s].end(); iterVex++)
			{
				sub_scene[s].mesh.vertices.push_back(iterVex->second);
				vexIdxold2New[s][iterVex->first] = numC;
				numC++;
			}

			for (int k = 0; k < sub_face[s].size(); k++)
			{
				int oldA = sub_face[s].at(k).x;
				int newA = vexIdxold2New[s][oldA];
				int oldB = sub_face[s].at(k).y;
				int newB = vexIdxold2New[s][oldB];
				int oldC = sub_face[s].at(k).z;
				int newC = vexIdxold2New[s][oldC];
				sub_scene[s].mesh.faces.AddConstruct(newA, newB, newC);
			}
		}

		for (int n = 0; n < num_block; n++)
		{
			sub_scene[n].images.Reserve(platforms.size());
			sub_scene[n].platforms.Reserve(platforms.size());
			sub_scene[n].nCalibratedImages = platforms.size();
			FOREACH(idx, platforms)
			{
				MVS::Image& sub_image = sub_scene[n].images.AddEmpty();
				sub_image.name = images[idx].name;
				//	sub_image.cameraID = k;
				if (!sub_image.ReloadImage(0, false)) {
					LOG("error: can not read image %s", sub_image.name.c_str());
					return EXIT_FAILURE;
				}
				sub_image.platformID = sub_scene[n].platforms.GetSize();
				MVS::Platform& sub_platform = sub_scene[n].platforms.AddEmpty();
				//MVS::Platform::Camera& sub_camera = sub_platform.cameras.AddEmpty();
				sub_platform.cameras.Reserve(platforms[idx].cameras.size());
				FOREACH(jj, platforms[idx].cameras)
				{
					MVS::Platform::Camera& sub_camera = sub_platform.cameras.AddEmpty();
					sub_camera = platforms[idx].cameras[jj];
				}
				sub_image.cameraID = 0;
				sub_platform.poses.Reserve(platforms[idx].poses.size());
				FOREACH(kk, platforms[idx].poses)
				{
					sub_image.poseID = sub_platform.poses.GetSize();
					MVS::Platform::Pose & sub_pose = sub_platform.poses.AddEmpty();
					sub_pose = platforms[idx].poses[kk];
				}
				sub_image.UpdateCamera(sub_scene[n].platforms);
			}
		}	
	}
	return true;
}

bool Scene::CutMeshScope()
{

	std::map<int, Point3f>sub_vertex;
	FaceList sub_face;
	std::map<int, Vec3f>sub_normal;

	std::map<int, int> vexIdxold2New;
	//vexIdxold2New.resize(num_block);
	int num_tri = mesh.faces.size();
	for (int i = 0; i < num_tri; i++)
	{
		//		Point3f PntA = mesh.faces[i];
		int A = mesh.faces[i].x;
		int B = mesh.faces[i].y;
		int C = mesh.faces[i].z;

		Point3f pntA = mesh.vertices[A];
		Point3f pntB = mesh.vertices[B];
		Point3f pntC = mesh.vertices[C];
		Point3f centerP = (pntA + pntB + pntC) / 3;

		if (centerP.x >= bb.m_xmin && centerP.x <= bb.m_xmax
			&& centerP.y >= bb.m_ymin  && centerP.y <= bb.m_ymax)
		{
			VertexID vt;
			vt.x = A;
			vt.y = B;
			vt.z = C;
			sub_face.push_back(vt);
		}
		else
			continue;

	}


	for (int g = 0; g < sub_face.size(); g++)
	{
		VertexID AA = sub_face[g];
		//sub_color[h][AA]=this->get_color_by_facenum(AA);
		sub_vertex[AA.x] = mesh.vertices[AA.x];
		sub_vertex[AA.y] = mesh.vertices[AA.y];
		sub_vertex[AA.z] = mesh.vertices[AA.z];
	}


	//mesh.EmptyExtra();
	mesh.vertices.Empty();
	mesh.faces.Empty();
	std::map<int, Point3f>::iterator iterVex = sub_vertex.begin();
	int numC = 0;
	for (; iterVex != sub_vertex.end(); iterVex++)
	{
		mesh.vertices.push_back(iterVex->second);
		vexIdxold2New[iterVex->first] = numC;
		numC++;
	}

	for (int k = 0; k < sub_face.size(); k++)
	{
		int oldA = sub_face[k].x;
		int newA = vexIdxold2New[oldA];
		int oldB = sub_face[k].y;
		int newB = vexIdxold2New[oldB];
		int oldC = sub_face[k].z;
		int newC = vexIdxold2New[oldC];
		mesh.faces.AddConstruct(newA, newB, newC);
	}

	return true;
}

// export all estimated cameras in a MeshLab MLP project as raster layers
bool Scene::ExportCamerasMLP(const String& fileName, const String& fileNameScene) const
{
	static const char mlp_header[] =
		"<!DOCTYPE MeshLabDocument>\n"
		"<MeshLabProject>\n"
		" <MeshGroup>\n"
		"  <MLMesh label=\"%s\" filename=\"%s\">\n"
		"   <MLMatrix44>\n"
		"1 0 0 0 \n"
		"0 1 0 0 \n"
		"0 0 1 0 \n"
		"0 0 0 1 \n"
		"   </MLMatrix44>\n"
		"  </MLMesh>\n"
		" </MeshGroup>\n";
	static const char mlp_raster[] =
		"  <MLRaster label=\"%s\">\n"
		"   <VCGCamera TranslationVector=\"%0.6g %0.6g %0.6g 1\""
		" LensDistortion=\"%0.6g %0.6g\""
		" ViewportPx=\"%u %u\""
		" PixelSizeMm=\"1 %0.4f\""
		" FocalMm=\"%0.4f\""
		" CenterPx=\"%0.4f %0.4f\""
		" RotationMatrix=\"%0.6g %0.6g %0.6g 0 %0.6g %0.6g %0.6g 0 %0.6g %0.6g %0.6g 0 0 0 0 1\"/>\n"
		"   <Plane semantic=\"\" fileName=\"%s\"/>\n"
		"  </MLRaster>\n";

	Util::ensureDirectory(fileName);
	File f(fileName, File::WRITE, File::CREATE | File::TRUNCATE);

	// write MLP header containing the referenced PLY file
	f.print(mlp_header, Util::getFileName(fileNameScene).c_str(), MAKE_PATH_REL(WORKING_FOLDER_FULL, fileNameScene).c_str());

	// write the raster layers
	f <<  " <RasterGroup>\n";
	FOREACH(i, images) {
		const Image& imageData = images[i];
		// skip invalid, uncalibrated or discarded images
		if (!imageData.IsValid())
			continue;
		const Camera& camera = imageData.camera;
		f.print(mlp_raster,
			Util::getFileName(imageData.name).c_str(),
			-camera.C.x, -camera.C.y, -camera.C.z,
			0, 0,
			imageData.width, imageData.height,
			camera.K(1,1)/camera.K(0,0), camera.K(0,0),
			camera.K(0,2), camera.K(1,2),
			 camera.R(0,0),  camera.R(0,1),  camera.R(0,2),
			-camera.R(1,0), -camera.R(1,1), -camera.R(1,2),
			-camera.R(2,0), -camera.R(2,1), -camera.R(2,2),
			MAKE_PATH_REL(WORKING_FOLDER_FULL, imageData.name).c_str()
		);
	}
	f << " </RasterGroup>\n</MeshLabProject>\n";

	return true;
} // ExportCamerasMLP

Soption::Soption(void) {
	//m_level = 1;          m_csize = 2;
	//m_threshold = 0.7;    m_wsize = 7;
	//m_minImageNum = 3;    m_CPU = 4;

}

void Soption::init( const std::string option) {

	std::ifstream ifstr;
	std::string optFile =  option;
	ifstr.open(optFile.c_str());
	while (1)
	{
		std::string name;
		ifstr >> name;
		if (ifstr.eof())
			break;
		if (name[0] == '#')
		{
			char buffer[1024];
			ifstr.putback('#');
			ifstr.getline(buffer, 1024);
			continue;
		}
		if (name == "cluster_num")  ifstr >> clusterNum >> sec_clusterNum;
		else if (name == "cluster_name")             ifstr >> inputSceneFilePath;
		else if (name == "densy_name")             ifstr >> densifyFilePath;
		else if (name == "mesh_name")             ifstr >> meshFilePath;
		else if (name == "refine_name")             ifstr >> refineFilePath;
		else if (name == "texture_name")             ifstr >> textureFilePath;
		else if (name == "work_ford")        ifstr >> workForder;
		else if (name == "norm_ford")        ifstr >> normForder;
		else if (name == "cut")        ifstr >> cut;
		else if (name == "scopes")   ifstr >> scopeH[0]>> scopeL[0] >> scopeH[1] >>scopeL[1] >>  scopeH[2]>> scopeL[2] ;

		else
		{
			std::cerr << "无法识别的选项: " << name << std::endl;
			break;
		}
	}
	ifstr.close();
}

/*----------------------------------------------------------------*/
