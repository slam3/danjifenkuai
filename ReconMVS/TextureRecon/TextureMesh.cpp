#include "MVS/Common.h"
#include "MVS/Scene.h"
#include <boost/program_options.hpp>
#include<direct.h>
using namespace MVS;


// D E F I N E S ///////////////////////////////////////////////////

#define APPNAME _T("TextureMesh")

// S T R U C T S ///////////////////////////////////////////////////

namespace OPT {
	String optFileName;
	int col;
	int row;
	int cut;
	int cluserNum;
	int sec_Num;
	String strOutputFile;
	BoundBox box;
	String strInputFileName;
	String strOutputFileName;
	String strMeshFileName;
	String strClusterName;
	unsigned nResolutionLevel;
	unsigned nMinResolution;
	float fOutlierThreshold;
	float fRatioDataSmoothness;
	bool bGlobalSeamLeveling;
	bool bLocalSeamLeveling;
	unsigned nTextureSizeMultiple;
	unsigned nRectPackingHeuristic;
	uint32_t nColEmpty;
	unsigned nOrthoMapResolution;
	unsigned nArchiveType;
	int nProcessPriority;
	unsigned nMaxThreads;
	String strExportType;
	String strConfigFileName;
	boost::program_options::variables_map vm;
} // namespace OPT

// initialize and parse the command line parameters
bool Initialize(size_t argc, LPCTSTR* argv)
{
	// initialize log and console
	OPEN_LOG();
	OPEN_LOGCONSOLE();

	// group of options allowed only on command line
	boost::program_options::options_description generic("Generic options");
	generic.add_options()
		("help,h", "produce this help message")
		//("working-folder,w", boost::program_options::value<std::string>(&WORKING_FOLDER), "working directory (default current directory)")
		("config-file,c", boost::program_options::value<std::string>(&OPT::strConfigFileName)->default_value(APPNAME _T(".cfg")), "file name containing program options")
		("export-type", boost::program_options::value<std::string>(&OPT::strExportType)->default_value(_T("obj")), "file type used to export the 3D scene (ply or obj)")
		("archive-type", boost::program_options::value<unsigned>(&OPT::nArchiveType)->default_value(2), "project archive type: 0-text, 1-binary, 2-compressed binary")
		("process-priority", boost::program_options::value<int>(&OPT::nProcessPriority)->default_value(-1), "process priority (below normal by default)")
		("max-threads", boost::program_options::value<unsigned>(&OPT::nMaxThreads)->default_value(0), "maximum number of threads (0 for using all available cores)")
#if TD_VERBOSE != TD_VERBOSE_OFF
		("verbosity,v", boost::program_options::value<int>(&g_nVerbosityLevel)->default_value(
#if TD_VERBOSE == TD_VERBOSE_DEBUG
			3
#else
			2
#endif
		), "verbosity level")
#endif
		;

	// group of options allowed both on command line and in config file
	boost::program_options::options_description config("Texture options");
	config.add_options()
		("input-file,i", boost::program_options::value<std::string>(&OPT::optFileName), "input filename containing camera poses and image list")
		("hang-col,l", boost::program_options::value<int>(&OPT::col), "input filename containing camera poses and image list")
		("lie-row,r", boost::program_options::value<int>(&OPT::row), "input filename containing camera poses and image list")
		("output-file,o", boost::program_options::value<std::string>(&OPT::strOutputFileName), "output filename for storing the mesh")
		("resolution-level,s", boost::program_options::value<unsigned>(&OPT::nResolutionLevel)->default_value(1), "how many times to scale down the images before mesh refinement")
		("min-resolution", boost::program_options::value<unsigned>(&OPT::nMinResolution)->default_value(640), "do not scale images lower than this resolution")
		("outlier-threshold", boost::program_options::value<float>(&OPT::fOutlierThreshold)->default_value(6e-2f), "threshold used to find and remove outlier face textures (0 - disabled)")
		("cost-smoothness-ratio", boost::program_options::value<float>(&OPT::fRatioDataSmoothness)->default_value(0.75f), "ratio used to adjust the preference for more compact patches (1 - best quality/worst compactness, ~0 - worst quality/best compactness)")
		("global-seam-leveling", boost::program_options::value<bool>(&OPT::bGlobalSeamLeveling)->default_value(true), "generate uniform texture patches using global seam leveling")
		("local-seam-leveling", boost::program_options::value<bool>(&OPT::bLocalSeamLeveling)->default_value(true), "generate uniform texture patch borders using local seam leveling")
		("texture-size-multiple", boost::program_options::value<unsigned>(&OPT::nTextureSizeMultiple)->default_value(0), "texture size should be a multiple of this value (0 - power of two)")
		("patch-packing-heuristic", boost::program_options::value<unsigned>(&OPT::nRectPackingHeuristic)->default_value(0), "specify the heuristic used when deciding where to place a new patch (0 - best fit, 3 - good speed, 100 - best speed)")
		("empty-color", boost::program_options::value<uint32_t>(&OPT::nColEmpty)->default_value(0x00000000), "color used for faces not covered by any image")
		("orthographic-image-resolution", boost::program_options::value<unsigned>(&OPT::nOrthoMapResolution)->default_value(0), "orthographic image resolution to be generated from the textured mesh - the mesh is expected to be already geo-referenced or at least properly oriented (0 - disabled)")
		;

	// hidden options, allowed both on command line and
	// in config file, but will not be shown to the user
	boost::program_options::options_description hidden("Hidden options");
	hidden.add_options()
		("mesh-file", boost::program_options::value<std::string>(&OPT::strMeshFileName), "mesh file name to texture (overwrite the existing mesh)")
		;

	boost::program_options::options_description cmdline_options;
	cmdline_options.add(generic).add(config).add(hidden);

	boost::program_options::options_description config_file_options;
	config_file_options.add(config).add(hidden);

	boost::program_options::positional_options_description p;
	p.add("input-file", -1);

	try {
		// parse command line options
		boost::program_options::store(boost::program_options::command_line_parser((int)argc, argv).options(cmdline_options).positional(p).run(), OPT::vm);
		boost::program_options::notify(OPT::vm);
		INIT_WORKING_FOLDER;
		// parse configuration file
		std::ifstream ifs(MAKE_PATH_SAFE(OPT::strConfigFileName));
		if (ifs) {
			boost::program_options::store(parse_config_file(ifs, config_file_options), OPT::vm);
			boost::program_options::notify(OPT::vm);
		}
	}
	catch (const std::exception& e) {
		LOG(e.what());
		return false;
	}

	// initialize the log file
	OPEN_LOGFILE(MAKE_PATH(APPNAME _T("-") + Util::getUniqueName(0) + _T(".log")).c_str());

	// print application details: version and command line
	Util::LogBuild();
	LOG(_T("Command line:%s"), Util::CommandLineToString(argc, argv).c_str());

	/*Util::ensureValidPath(OPT::strInputFileName);
	Util::ensureUnifySlash(OPT::strInputFileName);
	if (OPT::vm.count("help") || OPT::strInputFileName.IsEmpty()) {
		boost::program_options::options_description visible("Available options");
		visible.add(generic).add(config);
		GET_LOG() << visible;
	}
	if (OPT::strInputFileName.IsEmpty())
		return false;*/
	OPT::strExportType = OPT::strExportType.ToLower() == _T("obj") ? _T(".obj") : _T(".ply");

	Soption option;
	option.init(OPT::optFileName);
	OPT::cluserNum = option.clusterNum;
	OPT::sec_Num = option.sec_clusterNum;
	OPT::strOutputFile = option.textureFilePath;
	OPT::strInputFileName = option.refineFilePath;
	OPT::strMeshFileName=Util::getFullFileName(option.refineFilePath)+_T(".ply");
	WORKING_FOLDER = option.workForder;
	WORKING_FORDER_NORMAL = option.normForder;
	OPT::box.m_xmax = option.scopeH[0];
	OPT::box.m_xmin = option.scopeL[0];
	OPT::box.m_ymax = option.scopeH[1];
	OPT::box.m_ymin = option.scopeL[1];
	OPT::box.m_zmax = option.scopeH[2];
	OPT::box.m_zmin = option.scopeL[2];
	OPT::cut = option.cut;
	OPT::strClusterName = option.inputSceneFilePath;
	// initialize optional options
	Util::ensureValidPath(OPT::strOutputFileName);
	Util::ensureUnifySlash(OPT::strOutputFileName);
	if (OPT::strOutputFileName.IsEmpty())
		OPT::strOutputFileName = Util::getFullFileName(OPT::strInputFileName) + _T("_texture.mvs");

	// initialize global options
	Process::setCurrentProcessPriority((Process::Priority)OPT::nProcessPriority);
#ifdef _USE_OPENMP
	if (OPT::nMaxThreads != 0)
		omp_set_num_threads(OPT::nMaxThreads);
#endif

#ifdef _USE_BREAKPAD
	// start memory dumper
	MiniDumper::Create(APPNAME, WORKING_FOLDER);
#endif
	return true;
}

// finalize application instance
void Finalize()
{
#if TD_VERBOSE != TD_VERBOSE_OFF
	// print memory statistics
	Util::LogMemoryInfo();
#endif

	CLOSE_LOGFILE();
	CLOSE_LOGCONSOLE();
	CLOSE_LOG();
}

int main(int argc, LPCTSTR* argv)
{
#ifdef _DEBUGINFO
	// set _crtBreakAlloc index to stop in <dbgheap.c> at allocation
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);// | _CRTDBG_CHECK_ALWAYS_DF);
#endif

	if (!Initialize(argc, argv))
		return EXIT_FAILURE;

	Scene scene(OPT::nMaxThreads);
	// load and texture the mesh
	if (!scene.Load(MAKE_PATH_SAFE(OPT::strClusterName)))
		return EXIT_FAILURE;

	const String baseFileName(MAKE_PATH_SAFE(Util::getFullFileName(OPT::strOutputFileName)));
	if (scene.mesh.IsEmpty()) {
		if (!scene.mesh.Load(OPT::strMeshFileName))
		{
		VERBOSE("error: empty initial mesh %s", (OPT::strMeshFileName).c_str());
		return EXIT_FAILURE;
		}
	}

	if (OPT::nOrthoMapResolution && !scene.mesh.textureDiffuse.empty()) {
		// the input mesh is already textured and an orthographic projection was requested
		goto ProjectOrtho;
	}

	{
		String model_path = MAKE_PATH(String("Models"));  //在工作目录下创建子块Block文件夹
		_mkdir(model_path.c_str());
		// compute mesh texture
		scene.SetBoundingBox(OPT::box);
		//scene.CreateBoundingBox();
		scene.CreateConBoundingBox(375);
		scene.CutMeshScope();
		std::vector<Scene> sub_scene;
		int col = OPT::col;
		int row = OPT::row;
		sub_scene.resize(col*row);
		if (OPT::cut)
		{

			scene.mesh.Clean(0.30f, 0.f, false, 0, 0, true);
			scene.Devide(sub_scene, col, row);
			//scene.mesh.Clean(1.0f, 0.f, false, 0, 0, true);
			if (scene.TextureMesh(OPT::nResolutionLevel, OPT::nMinResolution, OPT::fOutlierThreshold, OPT::fRatioDataSmoothness, OPT::bGlobalSeamLeveling, OPT::bLocalSeamLeveling, OPT::nTextureSizeMultiple, OPT::nRectPackingHeuristic, Pixel8U(OPT::nColEmpty),false))
			{
				int k = 0;
				for (int i = 0; i < col*row; i++)
				{
					TD_TIMER_START();
					if (!sub_scene[i].mesh.IsEmpty())
					{
						sub_scene[i].mesh.Clean(1.0f, 0.f, false, 0, 0, true);
						sub_scene[i].mesh.Save(Util::getFilePath(OPT::strOutputFile) + String::FormatString("scene%02d-%04d-%02d.ply", OPT::cluserNum, OPT::sec_Num, k));
						scene.mesh.ReleaseExtra();	
						scene.mesh.Load(Util::getFilePath(OPT::strOutputFile) + String::FormatString("scene%02d-%04d-%02d.ply", OPT::cluserNum, OPT::sec_Num, k));						
					//	if (scene.mesh.faces.size() > 50)
						if (!scene.TextureMesh(OPT::nResolutionLevel, OPT::nMinResolution, OPT::fOutlierThreshold, OPT::fRatioDataSmoothness, OPT::bGlobalSeamLeveling, OPT::bLocalSeamLeveling, OPT::nTextureSizeMultiple, OPT::nRectPackingHeuristic, Pixel8U(OPT::nColEmpty),true))
							continue;

						const String mesh_path(MAKE_PATH_SAFE(model_path + String::FormatString("/mesh%02d_%04d_%02d", OPT::cluserNum, OPT::sec_Num, k)));
						_mkdir(mesh_path.c_str());
						const String mesh_name(MAKE_PATH_SAFE(mesh_path + String::FormatString("/mesh%02d_%04d_%02d", OPT::cluserNum, OPT::sec_Num, k)));
						//scene.Save(Util::getFilePath(OPT::strOutputFile) + String::FormatString("scene%02d-%04d-%02d.mvs", OPT::cluserNum,OPT::sec_Num, k), (ARCHIVE_TYPE)OPT::nArchiveType);
						scene.mesh.ComputeNormalVertices();
						scene.mesh.ComputeNormalFaces();
						scene.mesh.Save(mesh_name + OPT::strExportType);
					}
					else
						continue;
					k++;
				}

			}
		}
		else
		{
			TD_TIMER_START();
			scene.mesh.Clean(0.3f, 0.f, false, 0, 0, true); // extra cleaning to remove non-manifold problems created by closing holes
			if (!scene.TextureMesh(OPT::nResolutionLevel, OPT::nMinResolution, OPT::fOutlierThreshold, OPT::fRatioDataSmoothness, OPT::bGlobalSeamLeveling, OPT::bLocalSeamLeveling, OPT::nTextureSizeMultiple, OPT::nRectPackingHeuristic, Pixel8U(OPT::nColEmpty)))
				return EXIT_FAILURE;
			const String mesh_path(MAKE_PATH_SAFE(model_path + String::FormatString("/mesh%d_%d", OPT::cluserNum, 0)));
			_mkdir(mesh_path.c_str());
			const String mesh_name(MAKE_PATH_SAFE(mesh_path + String::FormatString("/mesh%d_%d", OPT::cluserNum, 0)));
			scene.mesh.ComputeNormalVertices();
			scene.mesh.ComputeNormalFaces();
			scene.Save(OPT::strOutputFile, (ARCHIVE_TYPE)OPT::nArchiveType);
			scene.mesh.Save(mesh_name + OPT::strExportType);
		}

		//VERBOSE("Mesh texturing completed: %u vertices, %u faces (%s)", scene.mesh.vertices.GetSize(), scene.mesh.faces.GetSize(), TD_TIMER_GET_FMT().c_str());

		//// save the final mesh

	}


	if (OPT::nOrthoMapResolution) {
		// project mesh as an orthographic image
	ProjectOrtho:
		Image8U3 imageRGB;
		Image8U imageRGBA[4];
		Point3 center;
		scene.mesh.ProjectOrthoTopDown(OPT::nOrthoMapResolution, imageRGB, imageRGBA[3], center);
		Image8U4 image;
		cv::split(imageRGB, imageRGBA);
		cv::merge(imageRGBA, 4, image);
		image.Save(baseFileName + _T("_orthomap.png"));
		SML sml(_T("OrthoMap"));
		sml[_T("Center")].val = String::FormatString(_T("%g %g %g"), center.x, center.y, center.z);
		sml.Save(baseFileName + _T("_orthomap.txt"));
	}

	Finalize();
	return EXIT_SUCCESS;
}
/*----------------------------------------------------------------*/
