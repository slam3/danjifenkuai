#include "MVS/Common.h"
#include "MVS/Scene.h"
#include <boost/program_options.hpp>
#include<direct.h>
using namespace MVS;


// D E F I N E S ///////////////////////////////////////////////////

#define APPNAME _T("SceneRecon")


// S T R U C T S ///////////////////////////////////////////////////

namespace OPT {
int clusterNum;
String optFileName;
String strInputFileName;
String strOutputFileName;
String strOutputFile;
String strMeshFileName;
String strDenseConfigFileName;
int nArchiveType;
int nProcessPriority;
unsigned nMaxThreads;
String strConfigFileName;
BoundBox box;
boost::program_options::variables_map vm;
} // namespace OPT

// initialize and parse the command line parameters
bool Initialize(size_t argc, LPCTSTR* argv)
{
	// initialize log and console
	OPEN_LOG();
	OPEN_LOGCONSOLE();

	// group of options allowed only on command line
	boost::program_options::options_description generic("Generic options");
	generic.add_options()
		("help,h", "produce this help message")
	//	("working-folder,w", boost::program_options::value<std::string>(&WORKING_FOLDER), "working directory (default current directory)")
	//	("config-file,c", boost::program_options::value<std::string>(&OPT::strConfigFileName)->default_value(APPNAME _T(".cfg")), "file name containing program options")
		("archive-type", boost::program_options::value(&OPT::nArchiveType)->default_value(1), "project archive type: 0-text, 1-binary, 2-compressed binary")
		("process-priority", boost::program_options::value(&OPT::nProcessPriority)->default_value(-1), "process priority (below normal by default)")
		("max-threads", boost::program_options::value(&OPT::nMaxThreads)->default_value(0), "maximum number of threads (0 for using all available cores)")
		#if TD_VERBOSE != TD_VERBOSE_OFF
		("verbosity,v", boost::program_options::value(&g_nVerbosityLevel)->default_value(
			#if TD_VERBOSE == TD_VERBOSE_DEBUG
			3
			#else
			2
			#endif
			), "verbosity level")
		#endif
		;

	// group of options allowed both on command line and in config file
	unsigned nResolutionLevel;
	unsigned nMinResolution;
	unsigned nNumViews;
	unsigned nMinViewsFuse;
	unsigned nEstimateColors;
	unsigned nEstimateNormals;
	boost::program_options::options_description config("Densify options");
	config.add_options()
		("input-file,i", boost::program_options::value<std::string>(&OPT::optFileName), "input filename containing camera poses and image list")
		("output-file,o", boost::program_options::value<std::string>(&OPT::strOutputFileName), "output filename for storing the dense point-cloud")
		("resolution-level,r", boost::program_options::value<unsigned>(&nResolutionLevel)->default_value(1), "how many times to scale down the images before point cloud computation")
		("min-resolution", boost::program_options::value<unsigned>(&nMinResolution)->default_value(640), "do not scale images lower than this resolution")
		("number-views", boost::program_options::value<unsigned>(&nNumViews)->default_value(0), "number of views used for depth-map estimation (0 - all neighbor views available)")
		("number-views-fuse", boost::program_options::value<unsigned>(&nMinViewsFuse)->default_value(3), "minimum number of images that agrees with an estimate during fusion in order to consider it inlier")
		("estimate-colors", boost::program_options::value<unsigned>(&nEstimateColors)->default_value(1), "estimate the colors for the dense point-cloud")
		("estimate-normals", boost::program_options::value<unsigned>(&nEstimateNormals)->default_value(1), "estimate the normals for the dense point-cloud")
		;

	// hidden options, allowed both on command line and
	// in config file, but will not be shown to the user
	boost::program_options::options_description hidden("Hidden options");
	hidden.add_options()
		("dense-config-file", boost::program_options::value<std::string>(&OPT::strDenseConfigFileName), "optional configuration file for the densifier (overwritten by the command line options)")
		;

	boost::program_options::options_description cmdline_options;
	cmdline_options.add(generic).add(config).add(hidden);

	boost::program_options::options_description config_file_options;
	config_file_options.add(config).add(hidden);

	boost::program_options::positional_options_description p;
	p.add("input-file", -1);

	try {
		// parse command line options
		boost::program_options::store(boost::program_options::command_line_parser((int)argc, argv).options(cmdline_options).positional(p).run(), OPT::vm);
		boost::program_options::notify(OPT::vm);
		INIT_WORKING_FOLDER;
		// parse configuration file
		std::ifstream ifs(MAKE_PATH_SAFE(OPT::strConfigFileName));
		if (ifs) {
			boost::program_options::store(parse_config_file(ifs, config_file_options), OPT::vm);
			boost::program_options::notify(OPT::vm);
		}
	}
	catch (const std::exception& e) {
		LOG(e.what());
		return false;
	}

	// initialize the log file
	OPEN_LOGFILE(MAKE_PATH(APPNAME _T("-")+Util::getUniqueName(0)+_T(".log")).c_str());

	// print application details: version and command line
	Util::LogBuild();
	LOG(_T("Command line:%s"), Util::CommandLineToString(argc, argv).c_str());

	// validate input
	//Util::ensureValidPath(OPT::strInputFileName);
	//Util::ensureUnifySlash(OPT::strInputFileName);
	//if (OPT::vm.count("help") || OPT::strInputFileName.IsEmpty()) {
	//	boost::program_options::options_description visible("Available options");
	//	visible.add(generic).add(config);
	//	GET_LOG() << visible;
	//}
	//if (OPT::strInputFileName.IsEmpty())
	//	return false;
	Soption option;
	option.init(OPT::optFileName);
	OPT::strOutputFile = option.densifyFilePath;
	OPT::strInputFileName = option.inputSceneFilePath;
	WORKING_FOLDER = option.workForder;
	WORKING_FORDER_NORMAL = option.normForder;
	Util::ensureDirectory(WORKING_FORDER_NORMAL);
	_mkdir(WORKING_FORDER_NORMAL);
	OPT::box.m_xmax = option.scopeH[0];
	OPT::box.m_xmin = option.scopeL[0];
	OPT::box.m_ymax = option.scopeH[1];
	OPT::box.m_ymin = option.scopeL[1];
	OPT::box.m_zmax = option.scopeH[2];
	OPT::box.m_zmin = option.scopeL[2];
	// initialize optional options
	Util::ensureValidPath(OPT::strOutputFileName);
	Util::ensureUnifySlash(OPT::strOutputFileName);
	if (OPT::strOutputFileName.IsEmpty())
		OPT::strOutputFileName = Util::getFullFileName(OPT::strInputFileName) + _T("_dense.mvs");

	// init dense options
	OPTDENSE::init();
	const bool bValidConfig(OPTDENSE::oConfig.Load(OPT::strDenseConfigFileName));
	OPTDENSE::update();
	OPTDENSE::nResolutionLevel = nResolutionLevel;
	OPTDENSE::nMinResolution = nMinResolution;
	OPTDENSE::nNumViews = nNumViews;
	OPTDENSE::nMinViewsFuse = nMinViewsFuse;
	OPTDENSE::nEstimateColors = nEstimateColors;
	OPTDENSE::nEstimateNormals = nEstimateNormals;
	if (!bValidConfig)
		OPTDENSE::oConfig.Save(OPT::strDenseConfigFileName);

	// initialize global options
	Process::setCurrentProcessPriority((Process::Priority)OPT::nProcessPriority);
	#ifdef _USE_OPENMP
	if (OPT::nMaxThreads != 0)
		omp_set_num_threads(OPT::nMaxThreads);
	#endif

	#ifdef _USE_BREAKPAD
	// start memory dumper
	MiniDumper::Create(APPNAME, WORKING_FOLDER);
	#endif
	return true;
}

// finalize application instance
void Finalize()
{
	#if TD_VERBOSE != TD_VERBOSE_OFF
	// print memory statistics
	Util::LogMemoryInfo();
	#endif

	CLOSE_LOGFILE();
	CLOSE_LOGCONSOLE();
	CLOSE_LOG();
}

int main(int argc, LPCTSTR* argv)
{
	#ifdef _DEBUGINFO
	// set _crtBreakAlloc index to stop in <dbgheap.c> at allocation
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);// | _CRTDBG_CHECK_ALWAYS_DF);
	#endif
	if (!Initialize(argc, argv))
		return EXIT_FAILURE;

	Scene scene(OPT::nMaxThreads);
	// load and estimate a dense point-cloud
	if (!scene.Load(MAKE_PATH_SAFE(OPT::strInputFileName)))
		return EXIT_FAILURE;
	if (scene.pointcloud.IsEmpty()) {
		VERBOSE("error: empty initial point-cloud");
		return EXIT_FAILURE;
	}
	if (!scene.SetBoundingBox(OPT::box))
	{
		return EXIT_FAILURE;
		VERBOSE("Scene Bounding Box's Creation Failure!");
	}

	/*if (!scene.CreateSubBoundingBox(10))
	{
		return EXIT_FAILURE;
		VERBOSE("Scene Bounding Box's Creation Failure!");
	}
	if (!scene.CutScope())
	{
		return EXIT_FAILURE;
		VERBOSE("Densifying point-cloud Cut Scopes Failure!");
	}*/

	if (!scene.CreateSubBoundingBox(15))
	{
		return EXIT_FAILURE;
		VERBOSE("Scene Bounding Box's Creation Failure!");
	}

	if ((ARCHIVE_TYPE)OPT::nArchiveType != ARCHIVE_MVS) {
		TD_TIMER_START();
		if (!scene.DenseReconstruction(true))
			return EXIT_FAILURE;
		VERBOSE("Densifying point-cloud completed: %u points (%s)", scene.pointcloud.points.GetSize(), TD_TIMER_GET_FMT().c_str());
	}
	//scene.PointCloudFilter();
	if (!scene.CreateSubBoundingBox(12))
	{
		return EXIT_FAILURE;
		VERBOSE("Scene Bounding Box's Creation Failure!");
	}
	VERBOSE("Scene Bounding Box's Creation Failure!");
	Scene cut_scene;
	if (!scene.CutScope(cut_scene))
	{
		return EXIT_FAILURE;
		VERBOSE("Densifying point-cloud Cut Scopes Failure!");
	}
	// save the final mesh
	const String baseFileName(MAKE_PATH_SAFE(Util::getFullFileName(OPT::strOutputFile)));
	cut_scene.Save(OPT::strOutputFile, ARCHIVE_MVS);
	cut_scene.pointcloud.Save(baseFileName+_T(".ply"));
	//scene.mesh.Subdivide();
	#if TD_VERBOSE != TD_VERBOSE_OFF
	if (VERBOSITY_LEVEL > 2)
		cut_scene.ExportCamerasMLP(baseFileName+_T(".mlp"), baseFileName+_T(".ply"));
	#endif

	Finalize();
	return EXIT_SUCCESS;
}
/*----------------------------------------------------------------*/
