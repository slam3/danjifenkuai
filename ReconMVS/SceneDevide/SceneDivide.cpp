#include "MVS/Common.h"
#include "MVS/Scene.h"
#include <fstream>
#include <sstream>
#include<direct.h>
#include "iostream"
#include <boost/program_options.hpp>
using namespace MVS;


// D E F I N E S ///////////////////////////////////////////////////

#define APPNAME _T("SceneDivide")


// S T R U C T S ///////////////////////////////////////////////////

namespace OPT {
	int Num;
	int Mun;
	String strInputFileName;
	String strOutputFileName;
	String strMeshFileName;
	String strDenseConfigFileName;
	int nArchiveType;
	int nProcessPriority;
	int nCutRange;
	unsigned nMaxThreads;
	String strConfigFileName;
	boost::program_options::variables_map vm;
} // namespace OPT

  // initialize and parse the command line parameters
bool Initialize(size_t argc, LPCTSTR* argv)
{
	// initialize log and console
	OPEN_LOG();
	OPEN_LOGCONSOLE();

	// group of options allowed only on command line
	boost::program_options::options_description generic("Generic options");
	generic.add_options()
		("help,h", "produce this help message")

		("config-file,c", boost::program_options::value<std::string>(&OPT::strConfigFileName)->default_value(APPNAME _T(".cfg")), "file name containing program options")
		("archive-type", boost::program_options::value(&OPT::nArchiveType)->default_value(2), "project archive type: 0-text, 1-binary, 2-compressed binary")
		("process-priority", boost::program_options::value(&OPT::nProcessPriority)->default_value(-1), "process priority (below normal by default)")
		("max-threads", boost::program_options::value(&OPT::nMaxThreads)->default_value(0), "maximum number of threads (0 for using all available cores)")
#if TD_VERBOSE != TD_VERBOSE_OFF
		("verbosity,v", boost::program_options::value(&g_nVerbosityLevel)->default_value(
#if TD_VERBOSE == TD_VERBOSE_DEBUG
			3
#else
			2
#endif
		), "verbosity level")
#endif
		;

	// group of options allowed both on command line and in config file
	unsigned nResolutionLevel;
	unsigned nMinResolution;
	unsigned nNumViews;
	unsigned nMinViewsFuse;
	unsigned nEstimateColors;
	unsigned nEstimateNormals;
	boost::program_options::options_description config("GenDepth options");
	config.add_options()
		("working-folder,w", boost::program_options::value<std::string>(&WORKING_FOLDER), "working directory (default current directory)")
		("input-file,i", boost::program_options::value<std::string>(&OPT::strInputFileName), "input filename containing camera poses and image list")
		("first_devide_num,n", boost::program_options::value<int>(&OPT::Num)->default_value(45), "scene devide output num")
		("second_devide_num,m", boost::program_options::value<int>(&OPT::Mun)->default_value(0), "scene devide twice")
		("output-file,o", boost::program_options::value<std::string>(&OPT::strOutputFileName), "output filename for storing the dense point-cloud")
		("resolution-level,s", boost::program_options::value<unsigned>(&nResolutionLevel)->default_value(1), "how many times to scale down the images before point cloud computation")
		("recon-level,r", boost::program_options::value<int>(&OPT::nCutRange)->default_value(6), "how many scene range should be cut 3-10")
		("min-resolution", boost::program_options::value<unsigned>(&nMinResolution)->default_value(640), "do not scale images lower than this resolution")
		("number-views", boost::program_options::value<unsigned>(&nNumViews)->default_value(8), "number of views used for depth-map estimation (0 - all neighbor views available)")
		("number-views-fuse", boost::program_options::value<unsigned>(&nMinViewsFuse)->default_value(4), "minimum number of images that agrees with an estimate during fusion in order to consider it inlier")
		("estimate-colors", boost::program_options::value<unsigned>(&nEstimateColors)->default_value(1), "estimate the colors for the dense point-cloud")
		("estimate-normals", boost::program_options::value<unsigned>(&nEstimateNormals)->default_value(0), "estimate the normals for the dense point-cloud")
		;

	// hidden options, allowed both on command line and
	// in config file, but will not be shown to the user
	boost::program_options::options_description hidden("Hidden options");
	hidden.add_options()
		("dense-config-file", boost::program_options::value<std::string>(&OPT::strDenseConfigFileName), "optional configuration file for the densifier (overwritten by the command line options)")
		;

	boost::program_options::options_description cmdline_options;
	cmdline_options.add(generic).add(config).add(hidden);

	boost::program_options::options_description config_file_options;
	config_file_options.add(config).add(hidden);

	boost::program_options::positional_options_description p;
	p.add("input-file", -1);

	try {
		// parse command line options
		boost::program_options::store(boost::program_options::command_line_parser((int)argc, argv).options(cmdline_options).positional(p).run(), OPT::vm);
		boost::program_options::notify(OPT::vm);
		INIT_WORKING_FOLDER;
		// parse configuration file
		std::ifstream ifs(MAKE_PATH_SAFE(OPT::strConfigFileName));
		if (ifs) {
			boost::program_options::store(parse_config_file(ifs, config_file_options), OPT::vm);
			boost::program_options::notify(OPT::vm);
		}
	}
	catch (const std::exception& e) {
		LOG(e.what());
		return false;
	}

	// initialize the log file
	OPEN_LOGFILE(MAKE_PATH(APPNAME _T("-") + Util::getUniqueName(0) + _T(".log")).c_str());

	// print application details: version and command line
	Util::LogBuild();
	LOG(_T("Command line:%s"), Util::CommandLineToString(argc, argv).c_str());

	// validate input
	Util::ensureValidPath(OPT::strInputFileName);
	Util::ensureUnifySlash(OPT::strInputFileName);
	if (OPT::vm.count("help") || OPT::strInputFileName.IsEmpty()) {
		boost::program_options::options_description visible("Available options");
		visible.add(generic).add(config);
		GET_LOG() << visible;
	}
	if (OPT::strInputFileName.IsEmpty())
		return false;

	// initialize optional options
	Util::ensureValidPath(OPT::strOutputFileName);
	Util::ensureUnifySlash(OPT::strOutputFileName);
	if (OPT::strOutputFileName.IsEmpty())
		OPT::strOutputFileName = Util::getFullFileName(OPT::strInputFileName) + _T("_dense.mvs");

	// init dense options
	OPTDENSE::init();
	const bool bValidConfig(OPTDENSE::oConfig.Load(OPT::strDenseConfigFileName));
	OPTDENSE::update();
	OPTDENSE::nResolutionLevel = nResolutionLevel;
	OPTDENSE::nMinResolution = nMinResolution;
	OPTDENSE::nNumViews = nNumViews;
	OPTDENSE::nMinViewsFuse = nMinViewsFuse;
	OPTDENSE::nEstimateColors = nEstimateColors;
	OPTDENSE::nEstimateNormals = nEstimateNormals;
	if (!bValidConfig)
		OPTDENSE::oConfig.Save(OPT::strDenseConfigFileName);

	// initialize global options
	Process::setCurrentProcessPriority((Process::Priority)OPT::nProcessPriority);
#ifdef _USE_OPENMP
	if (OPT::nMaxThreads != 0)
		omp_set_num_threads(OPT::nMaxThreads);
#endif

#ifdef _USE_BREAKPAD
	// start memory dumper
	MiniDumper::Create(APPNAME, WORKING_FOLDER);
#endif
	return true;
}

// finalize application instance
void Finalize()
{
#if TD_VERBOSE != TD_VERBOSE_OFF
	// print memory statistics
	Util::LogMemoryInfo();
#endif

	CLOSE_LOGFILE();
	CLOSE_LOGCONSOLE();
	CLOSE_LOG();
}

int main(int argc, LPCTSTR* argv)
{
#ifdef _DEBUGINFO
	// set _crtBreakAlloc index to stop in <dbgheap.c> at allocation
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);// | _CRTDBG_CHECK_ALWAYS_DF);
#endif
	//argc = 5;
	//argv[1] = "-i";
	//argv[2] = "H:\\DaYanTa\\data\\scene.mvs";
	//argv[3] = "-w";
	//argv[4] = "H:\\DaYanTa\\data\\";
	if (!Initialize(argc, argv))
		return EXIT_FAILURE;

	Scene scene(OPT::nMaxThreads);
	// load and estimate a dense point-cloud
	if (!scene.Load(MAKE_PATH_SAFE(OPT::strInputFileName)))
		return EXIT_FAILURE;
	//if (scene.pointcloud.IsEmpty()) {
	//	VERBOSE("error: empty initial point-cloud");
	//	return EXIT_FAILURE;
	//}

	//BoundBox box2;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);

	if (!scene.CreateSuitBoundingBox(cloud, cloud_filtered))
	{
		VERBOSE("error: create boundingbox of scene fasle");
		return EXIT_FAILURE;
	}
	if (!scene.CreateConBoundingBox(-OPT::nCutRange))
	{
		VERBOSE("error: create sub_boundingbox of scene fasle");
		return EXIT_FAILURE;

	}
	if (OPT::Mun == 0)
	{
		int num_img = scene.images.size();

		float  ratio = scene.bb.Height() / scene.bb.Width();
		if (OPT::Num >= 2)
		{
			int numBlock = num_img *OPT::Num / 1000;
			int col, row;
			if (numBlock == 0)
				numBlock = 2;

			col = ceil(sqrt(numBlock / ratio));
			row = ceil(ratio*sqrt(numBlock / ratio));

			std::vector<Scene> sub_scene;
			sub_scene.resize(col*row);
			scene.Devide(sub_scene, col, row);
			String run_path = MAKE_PATH(String::FormatString("scene_run.bat"));  //在工作目录下创建子块Block文件夹；
			std::ofstream runstr;
			runstr.open(MAKE_PATH_SAFE(run_path).c_str());
			runstr << "GenDepth -w " << WORKING_FOLDER << " -i scene.mvs -r " << OPTDENSE::nResolutionLevel << " -o " << MAKE_PATH_SAFE(WORKING_FOLDER + "Normal") << std::endl;
			for (int i = 0; i < sub_scene.size(); i++)
			{
				if (sub_scene[i].pointcloud.points.size() > 100)
				{
					//  sub_scene[i].CreateBoundingBox();
					String block_path = MAKE_PATH(String::FormatString("Block_%04d", i));  //在工作目录下创建子块Block文件夹；
					String option_path = MAKE_PATH_FULL(block_path, String::FormatString("/option-%04d.opt", i));
					Util::ensureDirectory(block_path);
					Util::ensureValidPath(block_path);
					_mkdir(block_path.c_str());
					const String scene_name(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_%04d", i)));
					const String scene_name_d(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_d_%04d", i)));
					const String scene_name_m(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_m_%04d", i)));
					const String scene_name_r(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_r_%04d", i)));
					const String scene_name_t(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_t_%04d", i)));
					sub_scene[i].Save(scene_name + _T(".mvs"), ARCHIVE_MVS);
					sub_scene[i].pointcloud.Save(scene_name + _T(".ply"));
					std::ofstream ofstr;
					ofstr.open(MAKE_PATH_SAFE(option_path).c_str());
					ofstr << "# generated by BundleSegment XueAO" << std::endl
						<< "cluster_num " << 0 << ' ' << i << std::endl
						<< "cluster_name " << scene_name + _T(".mvs") << std::endl
						<< "densy_name " << scene_name_d + _T(".mvs") << std::endl
						<< "mesh_name " << scene_name_m + _T(".mvs") << std::endl
						<< "refine_name " << scene_name_r + _T(".mvs") << std::endl
						<< "texture_name " << scene_name_t + _T(".mvs") << std::endl
						<< "work_ford " << WORKING_FOLDER << std::endl
						<< "norm_ford " << MAKE_PATH_SAFE(WORKING_FOLDER + "/Normal") << std::endl
						<< "cut " << 1 << std::endl;
					ofstr << "scopes " << ' ';
					ofstr << sub_scene[i].bb.m_xmax << ' ' << sub_scene[i].bb.m_xmin << ' ' << sub_scene[i].bb.m_ymax << ' '
						<< sub_scene[i].bb.m_ymin << ' ' << sub_scene[i].bb.m_zmax << ' ' << sub_scene[i].bb.m_zmin << ' ';
					ofstr << std::endl;
					ofstr.close();
					/*	  _mkdir(MAKE_PATH_SAFE(block_path + "/Normal").c_str());*/

					runstr << "SceneRecon -r " << OPTDENSE::nResolutionLevel << " -i " << option_path << std::endl;
					runstr << "MeshRecon -s 0.22 -k 15 -i " << option_path << std::endl;
					runstr << "MeshRefine -r " << OPTDENSE::nResolutionLevel << " -k 15 -i " << option_path << std::endl;
					runstr << "TextureRecon -i " << option_path << " -s " << OPTDENSE::nResolutionLevel << " -l " << 2 << " -r " << 2 << std::endl;
				}
				else
				{
					VERBOSE("sub_scene %d is empty", i);
					continue;
				}


			}
			runstr.close();
		}
		else
		{

			String block_path = MAKE_PATH(String::FormatString("Block_%04d", 0));  //在工作目录下创建子块Block文件夹；
			String option_path = MAKE_PATH_FULL(block_path, String::FormatString("/option-%04d.opt", 0));
			Util::ensureDirectory(block_path);
			Util::ensureValidPath(block_path);
			_mkdir(block_path.c_str());
			const String scene_name(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_%04d", 0)));
			const String scene_name_d(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_d_%04d", 0)));
			const String scene_name_m(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_m_%04d", 0)));
			const String scene_name_r(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_r_%04d", 0)));
			const String scene_name_t(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_t_%04d", 0)));
			scene.Save(scene_name + _T(".mvs"), (ARCHIVE_TYPE)OPT::nArchiveType);
			scene.pointcloud.Save(scene_name + _T(".ply"));
			std::ofstream ofstr;
			ofstr.open(MAKE_PATH_SAFE(option_path).c_str());
			ofstr << "# generated by BundleSegment XueAO" << std::endl
				<< "cluster_num " << 0 << ' ' << 0 << std::endl
				<< "cluster_name " << scene_name + _T(".mvs") << std::endl
				<< "densy_name " << scene_name_d + _T(".mvs") << std::endl
				<< "mesh_name " << scene_name_m + _T(".mvs") << std::endl
				<< "refine_name " << scene_name_r + _T(".mvs") << std::endl
				<< "texture_name " << scene_name_t + _T(".mvs") << std::endl
				<< "work_ford " << WORKING_FOLDER << std::endl
				<< "norm_ford " << MAKE_PATH_SAFE(block_path + "/Normal") << std::endl
				<< "cut " << 0 << std::endl;
			ofstr << "scopes " << ' ';
			ofstr << scene.bb.m_xmax + 1 << ' ' << scene.bb.m_xmin - 1 << ' ' << scene.bb.m_ymax + 1 << ' '
				<< scene.bb.m_ymin - 1 << ' ' << scene.bb.m_zmax + 1 << ' ' << scene.bb.m_zmin - 1 << ' ';
			ofstr << std::endl;
			ofstr.close();
			_mkdir(MAKE_PATH_SAFE(block_path + "/Normal").c_str());

			String run_path = MAKE_PATH(String::FormatString("scene_run.bat"));  //在工作目录下创建子块Block文件夹；
			std::ofstream runstr;
			runstr.open(MAKE_PATH_SAFE(run_path).c_str());

			runstr << "SceneRecon -i " << option_path << " -r " << 0 << std::endl;
			runstr << "MeshRecon -s 0.25 -i " << option_path << std::endl;
			runstr << "MeshRefine -r 0 -i " << option_path << std::endl;
			runstr << "TextureRecon -i " << option_path << " -s " << 0 << std::endl;
			runstr.close();
		}
	}
	else
	{
		int sec_block_num = 4;
		float  ratio = scene.bb.Height() / scene.bb.Width();

		int sec_col = round(sqrt(sec_block_num / ratio));
		int sec_row = round(ratio*sqrt(sec_block_num / ratio));


		if (sec_col == 0)
			sec_col = sec_col + 1;

		int num_xx = sec_col*sec_row;
		if (sec_row == 0 || num_xx == 1)
			sec_row = sec_row + 1;

		int sec_block = sec_col*sec_row;

		std::vector<Scene> sub_scene;
		sub_scene.resize(sec_block);

		scene.Devide(sub_scene, sec_col, sec_row);  //直接进行第一次分块

		std::vector<std::vector<Scene>> sec_sub_scene;  //准备进行第二次分块；
		sec_sub_scene.resize(sec_block);

		String run_path = MAKE_PATH(String::FormatString("scene_run.bat"));  //在工作目录下创建子块Block文件夹；
		std::ofstream runstr;
		runstr.open(MAKE_PATH_SAFE(run_path).c_str());

		for (int i = 0; i < sec_block; i++)
		{

			const String scene_name(MAKE_PATH(String::FormatString("scene_%04d", i)));
			sub_scene[i].Save(scene_name + _T(".mvs"), ARCHIVE_MVS);
			sub_scene[i].pointcloud.Save(scene_name + _T(".ply"));//保存第一次分块结果；

			String scene_file_name = String::FormatString("scene_%04d", i) + _T(".mvs");
			String normal_dir = String::FormatString("normal_%04d", i);

			runstr << "GenDepth -w " << WORKING_FOLDER << " -i " << scene_file_name << " -r " << OPTDENSE::nResolutionLevel << " -o " << MAKE_PATH_SAFE(WORKING_FOLDER + normal_dir) << std::endl;

			int sec_img_num = sub_scene[i].images.size();
			float  sec_ratio = sub_scene[i].bb.Height() / sub_scene[i].bb.Width();
			int sec_numBlock = sec_img_num *OPT::Num / 1000;   //设置第二次分块的块数

			int sec_d_col, sec_d_row;
			if (sec_numBlock == 0)
				sec_numBlock = 2;

			sec_d_col = ceil(sqrt(sec_numBlock / sec_ratio));
			sec_d_row = ceil(sec_ratio*sqrt(sec_numBlock / sec_ratio));
			sec_sub_scene[i].resize(sec_d_col*sec_d_row);

			if (sec_d_col*sec_d_row >= 2)
			{
				//	sub_scene[i].CreateConBoundingBox(30);
				sub_scene[i].Devide(sec_sub_scene[i], sec_d_col, sec_d_row);//进行第二次分块

				for (int j = 0; j < sec_sub_scene[i].size(); j++)
				{
					if (sec_sub_scene[i][j].pointcloud.points.size() > 80)
					{
						//  sub_scene[i].CreateBoundingBox();
						String block_path = MAKE_PATH(String::FormatString("Block_%02d_%04d", i, j));  //在工作目录下创建子块Block文件夹；
						String option_path = MAKE_PATH_FULL(block_path, String::FormatString("/option-%02d-%04d.opt", i, j)); //保存option文件
						Util::ensureDirectory(block_path);
						Util::ensureValidPath(block_path);
						_mkdir(block_path.c_str());
						const String sec_scene_name(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_%02d_%04d", i, j)));
						const String scene_name_d(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_d_%02d_%04d", i, j)));
						const String scene_name_m(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_m_%02d_%04d", i, j)));
						const String scene_name_r(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_r_%02d_%04d", i, j)));
						const String scene_name_t(MAKE_PATH_SAFE(block_path + String::FormatString("/scene_t_%02d_%04d", i, j))); //设置中间结果保存路径
						sec_sub_scene[i][j].Save(sec_scene_name + _T(".mvs"), ARCHIVE_MVS);
						sec_sub_scene[i][j].pointcloud.Save(sec_scene_name + _T(".ply"));  //保存第二次分块结果
						std::ofstream ofstr;
						ofstr.open(MAKE_PATH_SAFE(option_path).c_str());
						ofstr << "# generated by BundleSegment XueAO" << std::endl
							<< "cluster_num " << i << ' ' << j << std::endl
							<< "cluster_name " << sec_scene_name + _T(".mvs") << std::endl
							<< "densy_name " << scene_name_d + _T(".mvs") << std::endl
							<< "mesh_name " << scene_name_m + _T(".mvs") << std::endl
							<< "refine_name " << scene_name_r + _T(".mvs") << std::endl
							<< "texture_name " << scene_name_t + _T(".mvs") << std::endl
							<< "work_ford " << WORKING_FOLDER << std::endl
							<< "norm_ford " << MAKE_PATH_SAFE(WORKING_FOLDER + normal_dir) << std::endl
							<< "cut " << 1 << std::endl;
						ofstr << "scopes " << ' ';
						ofstr << sec_sub_scene[i][j].bb.m_xmax << ' ' << sec_sub_scene[i][j].bb.m_xmin << ' ' << sec_sub_scene[i][j].bb.m_ymax << ' '
							<< sec_sub_scene[i][j].bb.m_ymin << ' ' << sec_sub_scene[i][j].bb.m_zmax << ' ' << sec_sub_scene[i][j].bb.m_zmin << ' ';
						ofstr << std::endl;
						ofstr.close();
						/*	  _mkdir(MAKE_PATH_SAFE(block_path + "/Normal").c_str());*/

						runstr << "SceneRecon -r " << OPTDENSE::nResolutionLevel << " -i " << option_path << std::endl;
						runstr << "MeshRecon -s 0.28 -k 20 -i " << option_path << std::endl;
						runstr << "MeshRefine -r " << OPTDENSE::nResolutionLevel << " -k 20 -i " << option_path << std::endl;
						runstr << "TextureRecon -i " << option_path << " -s " << OPTDENSE::nResolutionLevel << " -l " << 2 << " -r " << 2 << std::endl;
					}
					else
					{
						VERBOSE("sub_scene %d_%d is empty", i, j);
						continue;
					}
				}
			}
		}
		runstr.close();
	}

	Finalize();
	return EXIT_SUCCESS;
}